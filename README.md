## Introduction

*CFMumps* is a set of ColdFusion components (CFCs) that allow ColdFusion applications to access MUMPS data residing in a GT.M/YottaDB database. It can use either a GT.M/YottaDB Java Interface binding for high-performance, in-process connections to the GT.M/YottaDB database, or the open-source M/Wire protocol, written by Rob Tweed of M/Gateway Developments Ltd., to connect via a TCP protocol socket to a GT.M/YottaDB or Cache' server. The former option provides many orders of magnitude superior performance and security, while the latter allows ColdFusion and GT.M/YottaDB to reside on different servers. Both connectors implement identical interfaces for programmers, and switching between the in-process binding and M/Wire only requires changing one setting in a provided INI file. *CFMumps* implements David Wicksell's JSON-M specification to support JSON transport of MUMPS global nodes that contain both data and child subscripts. Also provided is an experimental in-process connector using the InterSystems Cache Extreme API, and a REST connector that can connect to a Node.js web service backed by David Wicksell's Nodem binding or InterSystems' cache.node binding. Please note that cache.node is, in our opinion, a highly unreliable and unstable module in many cases, and as such, should be avoided for use in production environments.

## Credits

The lib.cfmumps.Global.getObject() and lib.cfmumps.Global.setObject() APIs were written with the extremely helpful input and assistance of David Wicksell, owner of Fourth Watch Software LC, whose JSON-M specification they have been verified to fully support.

The TCP/IP connector is supported by way of M/Wire, an open-source product developed by Rob Tweed of M/Gateway Developments Ltd.

## Copyright and License

*CFMumps* is Copyright (C) 2014, 2016 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## CFMumps Architecture and Components

*CFMumps* consists of three main ColdFusion components:

1. lib.TCPSocket: Provides an object-oriented abstraction to java.net.Socket for network connectivity (used only by M/Wire connector)
2. lib.cfmumps.Mumps: Provides primitive database operations (GET, SET, KILL, ORDER, DATA, etc.) via GTMJI (lib.cfmumps.connector.GTMJI), M/Wire (lib.cfmumps.connector.MWire), a REST web service (lib.cfmumps.connector.REST), or Cache Extreme (lib.cfmumps.connector.CacheExtreme)
3. lib.cfmumps.Global: An object-oriented abstraction of a MUMPS global. Provides the ability to read MUMPS globals into CF structs, and write CF structs into MUMPS globals.

## Installation, Configuration, and Usage

Please refer to the [CFMumps Documentation](https://cfmumps.atlassian.net/wiki/display/CFM/CFMumps+Home) for installation, configuration, and usage information.