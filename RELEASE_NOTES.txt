RELEASE 0.8.0 (6 APR 2018)

* Add mumps_function_ex() and mumps_procedure_ex() to support MUMPS variables as
  arguments (either by reference or by value), automatically returning the 
  contents of the local symbol table as a CF struct [CFM-44]

RELEASE 0.7.0 (29 MAR 2018)
==============================================================================

* All CFMumps code has been re-written to use CFScript instead of 
  traditional CFML tags [CFM-25]

* BREAKING: All components can now be instantiated with the "new" operator. All
  existing applications must be changed to match. [CFM-26]

* APIs are refactored to use instance methods for string handling [CFM-27]

* Global.getObject() now returns numbers for node values where appropriate,
  whereas previously, all node values were returned as strings [CFM-29]

* CFMumps now supports a REST web service connector backed by Node.js and
  either NodeM or cache.node [CFM-30]

* The basic API now exposes atomic increment via increment() [CFM-31]

* The basic API now includes get_horolog() and convert_horolog() APIs to
  get the current $HOROLOG value and convert an horolog value to a date/time
  string [CFM-32]

* The GTMJI connector now allows developers to access and use MUMPS local 
  variables by preceding the global name in any API with a $ sign [CFM-33]

* The CFMumps test suite is now usable from the CLI or from a modern, elegant
  web UI [CFM-34]

* Changing connectors can now be accomplished via a shell script on the
  command line [CFM-35]

* A script has been added that will automatically download, build, and install
  GTMJI [CFM-36]

* API names are now consistently using underscores to separate words, though
  the old function names are preserved [CFM-37]

* CFMumps has dropped support for Adobe ColdFusion versions earlier 
  than CF 11 (we previously supported CF 8 or newer) [CFM-38]

* CFMumps has dropped support for all versions of Railo. Users are encouraged
  to update to Lucee, as Railo is effectively a dead project. [CFM-39]

* CFMumps connectors can now implement their own getObject and setObject. 
  This allows for potentially substantial performance improvements. This
  is a preliminary step to supporting forthcoming new features in the
  YottaDB MUMPS implementation, specifically the simple API [CFM-40]

* Component names are now capitalized, i.e., lib.cfmumps.Mumps,
  lib.cfmumps.Global, lib.cfmumps.Admin, lib.cfmumps.Util [CFM-41]

* Built-in connectors' names have changed as follows [CFM-42]:
    1) lib.cfmumps.connector.CacheExtreme
    2) lib.cfmumps.connector.GTMJI
    3) lib.cfmumps.connector.MWire
    4) lib.cfmumps.connector.REST

* Developers can now manually instantiate a connector to be used
  with the basic API, allowing multiple connectors to be used programmatically
  with in a single application. [CFM-43]


RELEASE 0.6.0 (26 JUL 2016)
==============================================================================

* CFMumps now uses semantic versioning in all releases. [CFM-24]

* Support for M/Wire has been reinstated. [CFM-23]

* The Cache connector has been corrected in order to properly implement
  the iMumpsConnector interface. [CFM-22]
  
* Via the lib.cfmumps.mProcedure() API, CFMumps now supports calling MUMPS 
  routines that do not return a value, without requiring the developer to
  implement an extrinsic function as a wrapper. 
  
  Note that this feature is only present in the GTMJI connector, as M/Wire
  cannot support it. The Cache connector may support it in future. [CFM-21]
  
RELEASE 0.05 (2 OCT 2014)
==============================================================================

* lib.cfmumps.mumps now verifies that the connector it is to use implements
  the iMumpsConnector interface and exposes public member variables
  connectorName and connectorVersion [CFM-17]
  
* Allow automatic relink of MUMPS routines [CFM-18]

* Remove support for M/Wire [CFM-19]

RELEASE 0.04 (26 AUG 2014)
==============================================================================

* Add alpha support for InterSystems Cache [CFM-12]

* Add administration application and default splash page [CFM-11]

RELEASE 0.03 (12 AUG 2014)
==============================================================================

* Add makefiles to automate builds [CFM-9]

* Custom tag supporting inline MUMPS code in CFML templates [CFM-6]

* Base support for Adobe ColdFusion 11. Removed all Railo vendor extensions
  from all components and replaced them with equivalents that work in both
  [CFM-5]

* lib.cfmumps.mumps validation of subscripts: component now throws an 
  exception on invalid use of empty subscripts instead of passing them to
  MUMPS [CFM-2]
  
* lib.cfmumps.mumps now has an mEval() method for passing back the output
  of a chunk of MUMPS code as a CF string [CFM-3]
  
* lib.cfmumps.global atomic operations: getObject() and setObject() now guard
  their operation with the use of incremental locking by default. The open()
  constructor now has an optional "atomic" argument as the last argument.
  The default value of this argument is "true", but if "false" is passed, the
  behavior of getObject() and setObject() will be identical to release 0.02
  [CFM-4]
  
* Implemented unit test suite in MXUnit framework. Tests are located at 
  tests/TestSuite.cfm [CFM-1]
 
RELEASE 0.02 (6 AUG 2014)
==============================================================================

* Initial public beta release
