<!---
cfmumps phonebook application demo

Copyright (C) 2014 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--->
<cfif isDefined("url.name")>
	<cfset entryGlobal = new lib.cfmumps.Global("phoneBook", [url.name])>
	<cfset entry = entryGlobal.getObject()>

	<cfset originalName = url.name>
	<cfset action = "Edit">
	<cfset name = url.name>
	<cfset eMail = entry.eMail>
	<cfset phoneNumber = entry.phoneNumber>
	<cfset address = entry.address>
	<cfset city = entry.city>
	<cfset state = entry.state>
	<cfset zip = entry.zip>		
	
	<cfset entryGlobal.close()>
<cfelse>
	<cfset action = "New">
	<cfset name = "">
	<cfset eMail = "">
	<cfset phoneNumber = "">
	<cfset address = "">
	<cfset city = "">
	<cfset state = "">
	<cfset zip = "">
</cfif>

<cfoutput>
<form class="form-horizontal" method="post" action="addContactSubmit.cfm">
<cfif isDefined("originalName")>
	<input type="hidden" name="originalName" id="originalName" value="#originalName#">
</cfif>
<fieldset>

<!-- Form Name -->
<legend>#action# Contact</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Contact Name</label>  
  <div class="col-md-6">
  <input id="name" name="name" type="text" placeholder="" class="form-control input-md" required="" value="#name#">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="eMail">E-Mail Address</label>  
  <div class="col-md-6">
  <input id="eMail" name="eMail" type="text" placeholder="" class="form-control input-md" value="#eMail#">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phoneNumber">Phone Number</label>  
  <div class="col-md-6">
  <input id="phoneNumber" name="phoneNumber" type="text" placeholder="" class="form-control input-md" value="#phoneNumber#">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Street Address</label>  
  <div class="col-md-5">
  <input id="address" name="address" type="text" placeholder="" class="form-control input-md" value="#address#">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="city">City</label>  
  <div class="col-md-4">
  <input id="city" name="city" type="text" placeholder="" class="form-control input-md" value="#city#">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="state">State</label>
  <div class="col-md-2">
    <select id="state" name="state" class="form-control">
      <option value="AL">AL</option>
      <option value="AK">AK</option>
      <option value="AZ">AZ</option>
      <option value="AR">AR</option>
      <option value="CA">CA</option>
      <option value="CO">CO</option>
      <option value="CT">CT</option>
      <option value="DE">DE</option>
      <option value="FL">FL</option>
      <option value="GA">GA</option>
      <option value="HI">HI</option>
      <option value="ID">ID</option>
      <option value="IL">IL</option>
      <option value="IN">IN</option>
      <option value="IA">IA</option>
      <option value="KS">KS</option>
      <option value="KY">KY</option>
      <option value="LA">LA</option>
      <option value="ME">ME</option>
      <option value="MD">MD</option>
      <option value="MA">MA</option>
      <option value="MI">MI</option>
      <option value="MN">MN</option>
      <option value="MS">MS</option>
      <option value="MO">MO</option>
      <option value="MT">MT</option>
      <option value="NE">NE</option>
      <option value="NV">NV</option>
      <option value="NH">NH</option>
      <option value="NJ">NJ</option>
      <option value="NM">NM</option>
      <option value="NY">NY</option>
      <option value="NC">NC</option>
      <option value="ND">ND</option>
      <option value="OH">OH</option>
      <option value="OK">OK</option>
      <option value="OR">OR</option>
      <option value="PA">PA</option>
      <option value="RI">RI</option>
      <option value="SC">SC</option>
      <option value="SD">SD</option>
      <option value="TN">TN</option>
      <option value="TX">TX</option>
      <option value="UT">UT</option>
      <option value="VT">VT</option>
      <option value="VA">VA</option>
      <option value="WA">WA</option>
      <option value="WV">WV</option>
      <option value="WI">WI</option>
      <option value="WY">WY</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="zip">ZIP</label>  
  <div class="col-md-2">
  <input id="zip" name="zip" type="text" placeholder="" class="form-control input-md" value="#zip#">
    
  </div>
</div>
<div class="form-group">
  <div class="col-md-10">
  <button type="submit" class="btn btn-primary pull-right">Save Contact</button>
</div>
</fieldset>
</form>
</cfoutput>
