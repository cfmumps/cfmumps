<!---
cfmumps phonebook application demo

Copyright (C) 2014, 2018 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--->
<cfset c = {}>
<cfset c.address = form.address>
<cfset c.city = form.city>
<cfset c.state = form.state>
<cfset c.zip = form.zip>
<cfset c.phoneNumber = form.phoneNumber>
<cfset c.eMail = form.eMail>

<cfset glob = new lib.cfmumps.Global("phoneBook", [form.name])>
<cfset glob.setObject(c)>
<cfset glob.close()>

<cfif isDefined("form.originalName")>
	<cfset origGlob = new lib.cfmumps.Global("phoneBook", [form.originalName])>
	<cfset origGlob.delete()>
	<cfset origGlob.close()>
</cfif>
<cflocation url="default.cfm" addtoken="false">


