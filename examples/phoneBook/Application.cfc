<cfcomponent displayName="Application" output="true">

	<cfset this.Name = "cfmumpsPhoneBook">
	<cfset this.ApplicationTimeout = CreateTimeSpan(0, 0, 1, 0)>
	<cfset this.SessionManagement = true>
	<cfset this.SetClientCookies = true>

	<cfsetting requesttimeout="500" showdebugoutput="false" enablecfoutputonly="false">

	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="true">
		

		<cfreturn true>
	</cffunction>

	<cffunction name="OnSessionStart" access="public" returntype="boolean" output="true">


		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="true">


		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequest" access="public" returntype="void" output="true">
		<cfargument name="TargetPage" type="string" required="true">

		<cfinclude template="#arguments.TargetPage#">	

		<cfif find("default.cfm", TargetPage)>

			<cfset mumps = new lib.cfmumps.Mumps()>
			<cfset mumps.open()>
			
			<footer class="footer">
				<cfoutput>
					Copyright &copy; 2014, 2018 Coherent Logic Development LLC<br>
					Provided under the terms of the GNU Affero General Public License v3<br>
					#mumps.version()#
				</cfoutput>
			</footer>

			<cfset mumps.close()>

		</cfif>

		<cfreturn>
	</cffunction>

	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true">

		<cfreturn>
	</cffunction>

	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false">
		<cfargument name="SessionScope" type="struct" required="true">
		<cfargument name="ApplicationScope" type="struct" required="true">

		<cfreturn>
	</cffunction>

	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false">
		<cfargument name="ApplicationScope" type="struct" required="false" default="#structNew()#">

		<cfreturn>
	</cffunction>

	<cffunction name="OnError" access="public" returntype="void" output="true">
		<cfargument name="Exception" type="any" required="true">
		<cfargument name="EventName" type="string" required="false" default="">

		<h1>Error</h1>
		<cfdump var="#Exception#">
		<cfreturn>
	</cffunction>
</cfcomponent>
