/*
 *cfmumps phonebook application demo
 *
 *Copyright (C) 2014 Coherent Logic Development LLC
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

function onReady() 
{
    $("#container").load("phoneBook.cfm", function() {
	$("#navNewContact").removeClass("active");
	$("#navHome").addClass("active");
    });

    return(true);
}

function addContact()
{
    $("#container").load("addContact.cfm", function() {
	$("#navHome").removeClass("active");
	$("#navNewContact").addClass("active");
    });
}

function editContact(name)
{
    var url = 'addContact.cfm?name=' + escape(name);

    $("#container").load(url, function() {
	$("#navHome").removeClass("active");
	$("#navNewContact").removeClass("active");
    });
}