<!---
cfmumps phonebook application demo

Copyright (C) 2014, 2018 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--->
<cfset entries = []>
<cfset index = 1>
<cfset db = new lib.cfmumps.Mumps()>
<cfset db.open()>

<cfset lastResult = false>
<cfset sub = "">

<cfloop condition="lastResult EQ false">
	<cfset order = db.order("phoneBook", [sub])>
	<cfset lastResult = order.lastResult>
	<cfset sub = order.value>

	<cfif sub NEQ "">
		<cfset entries[index] = sub>
		<cfset index += 1>
	</cfif>
</cfloop>

<cfset db.close()>

<table class="table table-striped">
	<tbody>
		<cfloop from="1" to="#entries.len()#" index="i">
			<cfset entryGlobal = new lib.cfmumps.Global("phoneBook", [entries[i]])>
			<cfset entry = entryGlobal.getObject()>

			<cfoutput>		
				<tr onclick="editContact('#entries[i]#')">					
					<td><strong>#entries[i]#</strong></td>
					<td>#entry.eMail#</td>
					<td>#entry.phoneNumber#</td>
					<td>#entry.address# #entry.city#, #entry.state# #entry.zip#</td>
				</tr>
			</cfoutput>

			<cfset entryGlobal.close()>
		</cfloop>
	</tbody>
</table>
