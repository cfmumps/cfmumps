<!---
    cfmumps CFML to GT.M adaptor
     Copyright (C) 2014 Coherent Logic Development LLC
--->
<cfcomponent output="false" implements="iMumpsConnector" extends="lib.cfmumps.admin">

	<cfset this.connectorName = "My Connector">
	<cfset this.connectorVersion = "0.01">

	<cfset this.connectorPath = "lib.cfmumps.<this-connector-name>">
	<cfset this.someSetting = this.getConfig(this.connectorPath, "someSetting")>

	<cffunction name="open" returntype="component" access="public" output="false">

	</cffunction>

	<cffunction name="isOpen" returntype="boolean" access="public" output="false">

	</cffunction>

	<cffunction name="close" returntype="component" access="public" output="false">

	</cffunction>

	<cffunction name="getSettings" returntype="void" access="public" output="true">

	</cffunction>

	<cffunction name="saveSettings" returntype="void" access="public" output="true">

	</cffunction>

	<cffunction name="set" returntype="void" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">
		<cfargument name="value" type="string" required="true">

	</cffunction>

	<cffunction name="get" returntype="any" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">

	</cffunction>

	<cffunction name="kill" returntype="void" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">

	</cffunction>

	<cffunction name="data" returntype="struct" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">

	</cffunction>

	<cffunction name="order" returntype="struct" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">

	</cffunction>

	<cffunction name="mquery" returntype="string" access="public" output="false">
		<cfargument name="globalRef" type="string" required="true">

	</cffunction>

	<cffunction name="merge" returntype="numeric" access="public" output="false">
		<cfargument name="inputGlobalName" type="string" required="true">
		<cfargument name="inputGlobalSubscripts" type="array" required="true">
		<cfargument name="outputGlobalName" type="string" required="true">
		<cfargument name="outputGlobalSubscripts" type="array" required="true">

	</cffunction>

	<cffunction name="lock" returntype="boolean" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">
		<cfargument name="timeout" type="numeric" required="true" default="0">

	</cffunction>

	<cffunction name="unlock" returntype="boolean" access="public" output="false">
		<cfargument name="globalName" type="string" required="true">
		<cfargument name="subscripts" type="array" required="true">

	</cffunction>

	<cffunction name="mVersion" returntype="string" access="public" output="false">

	</cffunction>

	<cffunction name="mFunction" returntype="any" access="public" output="false">
		<cfargument name="fn" type="string" required="true">
		<cfargument name="args" type="array" required="true">

	</cffunction>

</cfinterface>
