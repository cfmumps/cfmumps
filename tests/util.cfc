/*
 * CFMumps
 *
 * tests.util:		CFMumps utility API unit tests
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 12 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
component extends=testbox.system.BaseSpec
{
	function beforeTests()
	{

	}

	function afterTests()
	{

	}

	public void function testGetPiece()
	{
		var u = createObject("component", "lib.cfmumps.Util");
		var input = "foo^bar^bas";

		var r = u.getPiece(input, "^", 2);

		$assert.isEqual(r, "bar");

		input = "00^0100^300";
		var r = u.getPiece(input, "^", 1);

		$assert.isEqual(r, "00");

		var r = u.getPiece(input, "^", 2);

		$assert.isEqual(r, "0100");

		var r = u.getPiece(input, "^", 3);

		$assert.isEqual(r, "300");
	}

	public void function testSetPiece()
	{
		var u = createObject("component", "lib.cfmumps.Util");
		var input = "foo^bar^bas";

		var r = u.setPiece(input, "^", 2, "bat");

		$assert.isEqual("foo^bat^bas", r);
	}

	private void function testConvertFromFmDate()
	{
		var u = createObject("component", "lib.cfmumps.Util");

		var fmDate = "3170112.0100";
		debug("fmDate = " & fmDate);

		var r = u.convertFromFmDate(fmDate);

		debug("result = " & r);

		dateTimeStr = dateFormat(r, "m/d/yyyy") & " " & timeFormat(r, "H:mm");

		$assert.isEqual("1/12/2017 1:00", dateTimeStr);
	}

	public void function testConvertToFmDate()
	{
		var u = createObject("component", "lib.cfmumps.Util");

		var date = parseDateTime("1/12/2017 1:00");
		var r = u.convertToFmDate(date);

		$assert.isEqual("3170112.0100", r);
	}

	public void function testTitleCase()
	{
		var u = createObject("component", "lib.cfmumps.Util");
		var input = "this is a test";

		var r = u.titleCase(input);

		$assert.isEqual("This Is A Test", r);
	}
}