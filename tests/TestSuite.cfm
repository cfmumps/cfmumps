<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CFMumps Test Suite</title>
</head>
<body>
<cfscript>
	system = createObject("java", "java.lang.System");
	environment = system.getenv();

	gtm_dist = environment.get("gtm_dist");
	gtmroutines = environment.get("gtmroutines");
	gtmgbldir = environment.get("gtmgbldir");
	gtmci = environment.get("GTMCI");
	classpath = environment.get("CLASSPATH");
	ldlibpath = environment.get("LD_LIBRARY_PATH");

	ciFile = fileOpen(gtmci);
	ciLines = [];

	while(!fileIsEOF(ciFile)) {
		ciLines.append(fileReadLine(ciFile));
	}

	fileClose(ciFile);

</cfscript>
	<h1>CFMumps Test Suite</h1>
	<hr>
	<h2>Configuration</h2>
	<table>
	<tr>
		<th>Key</th>
		<th>Value</th>
	</tr>
	<cfoutput>
		<tr>
			<td>$gtm_dist</td>
			<td>#gtm_dist#</td>
		</tr>
		<tr>
			<td>$gtmroutines</td>
			<td>#gtmroutines#</td>
		</tr>
		<tr>
			<td>$gtmgbldir</td>
			<td>#gtmgbldir#</td>
		</tr>
		<tr>
			<td>$GTMCI</td>
			<td>#gtmci#</td>
		</tr>
		<tr>
			<td>$CLASSPATH</td>
			<td>#classpath#</td>
		</tr>
		<tr>
			<td>$LD_LIBRARY_PATH</td>
			<td>#ldlibpath#</td>
		</tr>

	</cfoutput>
	</table>
	<hr>
	<h2>GT.M Call-In Table</h2>
	<div>
		<cfscript>
			for(line in ciLines) {
				writeOutput(line & "<br>");
			}
		</cfscript>
	</div>
	<hr>
	<h2>Test Results</h2>

<cfset timeOut = 60 * 5>
<cfsetting requesttimeout="#timeOut#">
<cfscript>

	tests = ["tests.mumps",
	         "tests.global",
			 "tests.util",
			 "tests.performance"];

	writeOutput(new testbox.system.TestBox().run(bundles=tests, reporter="simple"));
</cfscript>
</body>
</html>