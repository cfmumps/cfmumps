/*
 * CFMumps
 *
 * tests.global:		CFMumps global API unit tests
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 12 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
component extends=testbox.system.BaseSpec
{

	function beforeTests()
	{

	}

	function afterTests()
	{

	}

	public void function testOpen()
	{
		success = false;

		try {
			glob = new lib.cfmumps.Global("KBBMTEST", []);
			glob.close();
			success = true;
		}
		catch (any ex) {
			success = false;
		}

		$assert.isTrue(success);
	}

	public void function testClose()
	{
		success = false;

		try {
			glob = new lib.cfmumps.Global("KBBMTEST", []);
			glob.close();
			success = true;
		}
		catch (any ex) {
			success = false;
		}

		$assert.isTrue(success);
	}

	public void function testValue()
	{
		glob = new lib.cfmumps.Global("KBBMTEST", ["global"]);
		glob.value("test");

		result = glob.value();

		glob.close();

		$assert.isEqual(result, "test");
	}

	public void function testDefined()
	{
		glob = new lib.cfmumps.Global("KBBMTEST", ["global"]);
		glob.value("test");

		result = glob.defined();

		glob.close();

		$assert.isTrue(result.defined);
	}

	public void function testDelete()
	{
		glob = new lib.cfmumps.Global("KBBMTEST", ["global"]);
		glob.value("test");

		success = false;

		try {
			glob.delete();
			success = true;
		}
		catch (any ex) {
			success = false;
		}

		glob.close();

		$assert.isTrue(success);
	}

	public void function testSetObject()
	{
		testObject = {
			sub1: "foo",
			sub2: {
				subsub1: "foo",
				subsub2: "bar"
			},
			sub3: "bar"
		};

		glob = new lib.cfmumps.Global("KBBMSTOB", []);
		glob.delete();

		glob.setObject(testObject);

		result = glob.getObject();

		$assert.isTrue(testObject.Equals(result));
	}

	public void function testGetObject()
	{
		testObject = {
			sub1: "foo",
			sub2: {
				subsub1: "foo",
				subsub2: "bar"
			},
			sub3: "bar"
		};

		glob = new lib.cfmumps.Global("KBBMSTOB", []);
		glob.delete();

		glob.setObject(testObject);

		result = glob.getObject();

		$assert.isTrue(testObject.Equals(result));
	}
}