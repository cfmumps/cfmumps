<cfoutput>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa #attributes.default_icon# fa-5x" id="icon_#attributes.test#"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge" id="status_#attributes.test#">Not Run</div>
						<div>#attributes.description#</div>
					</div>
				</div>
			</div>


			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-3 text-center">
						<div class="panel panel-success panel-horizontal">
							<div class="panel-heading">
								<div class="panel-title">
									<i class="fa fa-check fa-1x"></i>
								</div>
							</div>
							<div class="panel-body">
								<span style="color:black;" id="pass_#attributes.test#">0</span>
							</div>
						</div>
					</div>
					<div class="col-xs-3 text-center">
						<div class="panel panel-warning panel-horizontal">
							<div class="panel-heading">
								<div class="panel-title">
									<i class="fa fa-warning fa-1x"></i>
								</div>
							</div>
							<div class="panel-body">
								<span style="color:black;" id="fail_#attributes.test#">0</span>
							</div>
						</div>
					</div>
					<div class="col-xs-3 text-center">
						<div class="panel panel-danger panel-horizontal">
							<div class="panel-heading">
								<div class="panel-title">
									<i class="fa fa-bomb fa-1x"></i>
								</div>
							</div>
							<div class="panel-body">
								<span style="color:black;" id="error_#attributes.test#">0</span>
							</div>
						</div>
					</div>
					<div class="col-xs-3 text-center">
						<div class="panel panel-default panel-horizontal">
							<div class="panel-heading">
								<div class="panel-title">
									<i class="fa fa-clock-o fa-1x"></i>
								</div>
							</div>
							<div class="panel-body">
								<span style="color:black;" id="time_#attributes.test#">0</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
					<a href="##" onclick="runTests(['#attributes.test#']);">
						<span class="pull-left">Test #attributes.description#</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					</a>
					</div>
				</div> <!--- initial row --->
			</div> <!--- panel-footer --->

		</div>
	</div>
</cfoutput>

