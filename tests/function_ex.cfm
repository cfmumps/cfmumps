<cfscript>

mumps = new lib.cfmumps.Mumps();
mumps.open();

res = serializeJson(mumps.mumps_procedure_ex("GETS^DIQ", [200, "1,", "**", "", "newPerson"], true)); 

mumps.close();

</cfscript>

<h1>Code</h1>
<pre>
mumps = new lib.cfmumps.Mumps();
mumps.open();

res = serializeJson(mumps.mumps_procedure_ex("GETS^DIQ", [200, "1,", "**", "", "newPerson"], true)); 

mumps.close();
</pre>
<h1>Output (variable "res")</h1>


<cfoutput>
<pre>
#mumps.formatJson(res)#
</pre>
</cfoutput>
