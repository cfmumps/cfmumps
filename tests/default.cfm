<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>CFMumps Test Suite</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.0/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <style type="text/css">
            .navbar-brand,
            .navbar-brand li a {
                line-height: 75px;
                height: 75px;
            }
            .navbar-top-links li {
                line-height: 46px;
            }

            td {
                word-wrap: break-word;
            }

            table.dataTable tbody td {
                word-break: break-word;
                vertical-align: top;
            }
    </style>
	<cfscript>
        admin = new lib.cfmumps.Admin();
        connector = admin.getConfig("general", "connector");
        fw = new tests.Framework();
        connectors = fw.getConnectors();
    </cfscript>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="index.html"><img src="images/cfmumps.png" style="height: 100%; width: auto; float: left;"></a>
            </div>
            <div class="navbar-default" role="navigation">
                <div class="navbar-collapse">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="active" id="dashboard" role="button" tabindex="0"><i class="fa fa-cogs fa-fw"></i> Test Suite</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-12"></div>
            </div>
            <div class="row">
				<cfmodule template="testbar.cfm" test="mumps" default_icon="fa-database" description="Basic API">
				<cfmodule template="testbar.cfm" test="global" default_icon="fa-cloud" description="Global API">
				<cfmodule template="testbar.cfm" test="util" default_icon="fa-wrench" description="Utilities API">
				<cfmodule template="testbar.cfm" test="performance" default_icon="fa-rocket" description="Performance">
			</div>
            <div id="page-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-sliders fa-fw"></i> System Configuration
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered" id="tbl-environment">
                                    <thead>
                                        <tr>
                                            <th>Variable</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div><!-- panel-body -->
                        </div><!-- panel -->
                    </div><!-- col-lg-4 -->
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-server fa-fw"></i> Test Results
                            </div><!-- panel-heading -->
                            <div class="panel-body">
                                <div class="btn-group">
                                    <button class="btn btn-primary" id="run-all-tests-button" onclick="runAllTests();" style="margin-bottom: 8px;">Run All Tests</button> <button class="btn btn-primary" id="reset-tests-button" onclick="resetTests();" style="margin-bottom: 8px;">Reset</button>
                                    <div class="btn-group">
                                        <button class="btn btn-primary" type="button"><span id="currentConnector"><cfoutput>#connector#</cfoutput></span></button> <button aria-expanded="false" aria-haspopup="true" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"><span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button>
                                        <ul class="dropdown-menu">
                                            <li>
												<cfloop array="#connectors#" index="c">
													<cfoutput>
                                                		<a href="##" onclick="javascript:setConnector('#c#');">#c#</a>
													</cfoutput>
												</cfloop>
                                            </li>
                                        </ul>
                                    </div><!--- btn-group --->
                                </div><!--- btn-group --->
                                <table class="table table-striped table-bordered" id="tbl-test-results">
                                    <thead>
                                        <tr>
                                            <th><i class="fa fa-sun-o fa-1x"></i></th>
                                            <th>Test</th>
                                            <th>Output</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div><!-- panel-body -->
                        </div><!-- panel -->
                    </div><!-- col-lg-8 -->
                </div><!-- row -->
            </div><!-- page-content -->
        </div><!-- page-wrapper -->
    </div><!-- wrapper -->
    <footer>
        <div class="panel-footer text-center">
            <p class="muted credit">Copyright &copy; 2017 Coherent Logic Development LLC</p>
        </div>
    </footer>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
    <script src="//cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.0/datatables.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(init);
    </script>
</body>
</html>