/*
 * CFMumps
 *
 * tests.Framework:		CFMumps testing framework
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 25 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
component access=remote extends=lib.cfmumps.Admin output=false {

	public tests.Framework function init(struct options) output=false
	{
		if(isDefined("arguments.options")) {
			this.options = arguments.options;
		}
		else {
			this.options = {};
		}

		return this;
	}

	remote function getConnectors() output=false returnFormat="JSON"
	{
		var admin = new lib.cfmumps.Admin();
		var connectors = listToArray(admin.getConfig("general", "connectors"));

		return connectors;
	}

	remote function setConnector(required string connector) output=false returnFormat="JSON"
	{
		var admin = new lib.cfmumps.Admin();
		var connectors = listToArray(admin.getConfig("general", "connectors"));
		var found = false;

		for(element in connectors) {
			if(connector == element) {
				found = true;
			}
		}

		if(!found) {
			return {success: false, newConnector: connector};
		}

		admin.setConfig("general", "connector", connector);

		return {success: true, newConnector: connector};
	}

	remote function runTest(required string test) output=false returnFormat="JSON"
	{
		var tests = [test];
		var tb = new testbox.system.TestBox();
		var rawData = tb.runRaw(bundles=tests);
		var result = {};

		result.raw = deserializeJSON(serializeJSON(rawData));

		if(result.raw.totalFail > 0 OR result.raw.totalError > 0) {
			result.success = false;
		}
		else {
			result.success = true;
		}

		return result;
	}

	remote function getEnvironment() output=false returnFormat="JSON"
	{
		var system = createObject("java", "java.lang.System");
		var environment = system.getenv();
		var result = {};
		result.data = [];

		for(key in environment) {
			result.data.append({Key: key, Value: environment[key]});
		}

		return result;
	}
}