/*
 * CFMumps
 *
 * app.js:		CFMumps Test Runner
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 26 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
var dtTestResults; 
function init()
{
	initializers = [initEnvironment, initTestResults];
	
	for(i in initializers) {
		initializers[i]();
	}
}

function initEnvironment()
{
	$("#tbl-environment").DataTable({autoWidth: false, ajax: "/tests/Framework.cfc?method=getEnvironment",
	columns: [
	{data: 'Key',
	width: "100px"},
	{data: 'Value',
	width: "200px"}]});
}

function initTestResults() 
{
	dtTestResults = $("#tbl-test-results").DataTable({language: {emptyTable: "Please run some tests"}, paging: false, autoWidth: false, columns: [
		{width: "40px"},
		{width: "auto"},
		{}
	]});	
}

function runAllTests() {
	var tests = ["mumps",
				  "global",
				  "util"];
				  
	runTests(tests);				 
}

function runTests(tests)
{
	for(test in tests) {		
		runTest(tests[test]);
	}	
}

function runTest(test)
{
	var testURL = "/tests/Framework.cfc?method=runTest&test=tests." + test;
	testProgressUpdate(test, "started");
	$.get(testURL, function(data) {
		
		if(data.success) {
			var status = "success";
		}
		else {
			var status = "failure";
		}
		
		testProgressUpdate(test, status, data.raw);
		processTestResult(test, data.raw.bundleStats[0].suiteStats[0].specStats, data.raw.bundleStats[0].debugBuffer);
	});
}

function resetTests()
{
	var tests = ["mumps",
				  "global",
				  "util",
				  "performance"];
				  
	for(i in tests) {
		testProgressUpdate(tests[i], "normal");
	}	
				  
	dtTestResults.clear().draw(false);
}

function processTestResult(test, specStats, debugBuffer)
{
	var output = {data: []};
	
	for(specIndex in specStats) {
		var spec = specStats[specIndex];
		var o = {};
		
		var omsg = "";
		
		for(i in debugBuffer) {
			msg = debugBuffer[i];
			if(msg.label === spec.name) {
				try {					
					perf = JSON.parse(msg.data);
					
					omsg = logPerformance(perf);
				}
				catch (ex) {
					console.log(ex);
					omsg = msg + " ";
				}
			}
		}
		
		o.test = "tests." + test + "." + spec.name;
		if(spec.status === "Passed") {
			o.status = '<i class="fa fa-circle fa-1x" style="color:green;"></i>';			
		}
		else {
			o.status = '<i class="fa fa-circle fa-1x" style="color:red;"></i>';			
		}
			
		o.output = omsg + spec.failMessage;
		
		dtTestResults.row.add([
			o.status,
			o.test,
			o.output
		]).draw(false);
	}
	
	
}

function logPerformance(perf)
{
	console.log(perf);
	
	return perf.performance.description;
}

function testProgressUpdate(test, status, raw)
{
	var passElem = "#pass_" + test;
	var failElem = "#fail_" + test;
	var errorElem = "#error_" + test;
	var timeElem = "#time_" + test;
	
	if(raw) {
		var pass = raw.totalPass;
		var fail = raw.totalFail;
		var error = raw.totalError;
		if(raw.totalDuration > 1000) {
			var time = raw.totalDuration / 1000;	
			time = Math.ceil(time);
			time = time.toString() + "s";
		}
		else {
			var time = raw.totalDuration + "ms";			
		}
	}
	else {
		var pass = 0;
		var fail = 0;
		var error = 0;
		var time = "0s";
	}
	
	$(passElem).html(pass);
	$(failElem).html(fail);
	$(errorElem).html(error);
	$(timeElem).html(time);
	
	var icons = {
		normal: {
			tests: {
				mumps: "fa fa-database fa-5x",
				global: "fa fa-cloud fa-5x",
				util: "fa fa-wrench fa-5x",
				performance: "fa fa-rocket fa-5x",
			}
		},
		started: {
			tests: {
				mumps: "fa fa-refresh fa-spin fa-5x fa-fw",
				global: "fa fa-refresh fa-spin fa-5x fa-fw",
				util: "fa fa-refresh fa-spin fa-5x fa-fw",
				performance: "fa fa-refresh fa-spin fa-5x fa-fw"
			}
		},
		success: {
			tests: {
				mumps: "fa fa-thumbs-up fa-5x",
				global: "fa fa-thumbs-up fa-5x",
				util: "fa fa-thumbs-up fa-5x",
				performance: "fa fa-thumbs-up fa-5x"
			}
		},
		failure: {
			tests: {
				mumps: "fa fa-thumbs-down fa-5x",
				global: "fa fa-thumbs-down fa-5x",
				util: "fa fa-thumbs-down fa-5x",
				performance: "fa fa-thumbs-down fa-5x"
			}
		}
	};
	
	var iconID = "#icon_" + test;
	var statusID = "#status_" + test;
		
	
	$(iconID).removeClass();
	$(iconID).addClass(icons[status].tests[test]);
	
	switch(status) {
		case "normal":
			$(statusID).html("Not Run");
			break;
		case "started":
			$(statusID).html("Running");
			break;	
		case "success":
			$(statusID).html("Pass");
			toastr.success(test + " passed");
			break;
		case "failure":
			$(statusID).html("Fail");
			toastr.error(test + " failed");
			break;		
	}
	
}

function setConnector(connector)
{
	var url = "/tests/Framework.cfc?method=setConnector&connector=" + connector;
	
	$.get(url, function (data) {
		$("#currentConnector").html(data.newConnector);
	});
}

