/*
 * CFMumps
 *
 * tests.mumps:		CFMumps basic API unit tests
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 12 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
component extends=testbox.system.BaseSpec
{

	function beforeTests()
	{

	}

	function afterTests()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		db.kill("KBBMTEST", []);

		db.close();
	}

	public void function testOpen()
	{
		db = new lib.cfmumps.Mumps();
		db.open();
		$assert.isTrue(db.isOpen());
		db.close();
	}

	public void function testClose()
	{
		db = new lib.cfmumps.Mumps();
		db.open();
		db.close();

		$assert.isFalse(db.isOpen());
	}

	public void function testSet()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		db.set("KBBMTEST", ["set"], "test");
		value = db.get("KBBMTEST", ["set"]);

		db.close();
		$assert.isEqual(value, "test");

		if(db.connector.connectorName != "M/Wire Connector") {
			this.setControlChars();

			var result = "";
			for(i = 1; i < 32; i++) {
				result = db.get("KBBMTEST", ["CONTROL", chr(i)]);

				$assert.isEqual(result, chr(i));
			}
		}

	}

	public void function testGet()
	{
		this.setSomething();

		db = new lib.cfmumps.Mumps();
		db.open();

		value = db.get("KBBMTEST", ["set"]);
		db.close();

		$assert.isEqual(value, "test");
	}

	public void function testIncrement()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		db.set("KBBMTEST", ["increment"], 1);
		value = db.increment("KBBMTEST", ["increment"]);
		db.close();

		$assert.isEqual(value, 2);
	}

	public void function testKill()
	{
		this.setSomething();

		db = new lib.cfmumps.Mumps();
		db.open();

		db.kill("KBBMTEST", ["set"]);

		data = db.data("KBBMTEST", ["set"]);
		db.close();
		$assert.isFalse(data.defined);
	}

	public void function testData()
	{
		this.setSomething();

		db = new lib.cfmumps.Mumps();
		db.open();

		data = db.data("KBBMTEST", ["set"]);

		db.close();

		$assert.isTrue(data.defined);
		$assert.isTrue(data.hasData);
		$assert.isFalse(data.hasSubscripts);
	}

	public void function testOrder()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		try {
			db.kill("KBBMTEST", ["ORDER"]);
		}
		catch (any ex) {

		}

		for(i = 1; i <= 10; i++) {
			db.set("KBBMTEST", ["ORDER", toString(i)], toString(i));
		}

		lastResult = false;
		nextSubscript = "";
		subs = ["ORDER", nextSubscript];

		subCount = 0;

		while(lastResult == false) {
			order = db.order("KBBMTEST", subs);

			lastResult = order.lastResult;
			nextSubscript = order.value;

			subs = ["ORDER", nextSubscript];

			if(nextSubscript != "") {
				subCount++;
			}
		}

		db.close();

		$assert.isEqual(subCount, 10);
	}

	public void function testQuery()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		db.kill("KBBMTQRY", []);
		db.set("KBBMTQRY", ["v", "x"], "test");

		result1 = db.query("^KBBMTQRY");
		result2 = db.query(result1);
		db.close();

		$assert.isEqual(result1, '^KBBMTQRY("v","x")');
		$assert.isEqual(result2, '');
	}

	public void function testMerge()
	{
		this.setSomething();

		db = new lib.cfmumps.Mumps();
		db.open();

		db.merge("KBBMTEST", [], "KBBMTST2", []);

		result = db.get("KBBMTST2", ["set"]);

		db.close();

		$assert.isEqual(result, "test");

	}

	public void function testLock()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = db.lock("KBBMTEST", ["lock"], 5);

		unlockResult = db.unlock("KBBMTEST", ["lock"]);
		db.close();

		$assert.isTrue(result);
	}

	public void function testUnlock()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = db.lock("KBBMTEST", ["lock"], 5);

		unlockResult = db.unlock("KBBMTEST", ["lock"]);
		db.close();

		$assert.isTrue(unlockResult);
	}

	public void function testVersion()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = db.version();

		db.close();

		$assert.isTrue(len(result) > 0);
	}

	public void function testFunction()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = db.mumps_function("FNTEST^KBBMCIDT", []);

		db.close();

		$assert.isEqual(result, "Testing");
	}

	public void function testPid()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = isNumeric(db.pid());

		debug("PID: " & toString(db.pid()));

		db.close();

		$assert.isTrue(result);
	}

	public void function testProcedure()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		result = db.mumps_procedure("PROCTEST^KBBMCIDT", []);

		db.close();

		$assert.isEqual(result, true);
	}

	public void function setSomething()
	{
		db = new lib.cfmumps.Mumps();
		db.open();
		db.set("KBBMTEST", ["set"], "test");
		db.close();
	}

	public void function setControlChars()
	{
		db = new lib.cfmumps.Mumps();
		db.open();

		db.kill("KBBMTEST", ["CONTROL"]);

		for(i = 1; i < 32; i++) {
			db.set("KBBMTEST", ["CONTROL", chr(i)], chr(i));
		}

		db.close();
	}

}