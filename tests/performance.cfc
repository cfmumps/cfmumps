/*
 * CFMumps
 *
 * tests.performance:		CFMumps performance tests
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 12 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
component extends=testbox.system.BaseSpec
{

	function beforeTests()
	{

	}

	function afterTests()
	{

	}

	public void function testBigNodeSet()
	{
		var test = {};
		var nodes = 1000;

		var db = new lib.cfmumps.mumps();
		db.open();

		var startTime = now();

		for(i = 1; i <= nodes; i++) {
			db.set("KBBMTEST", ["bigset", i], i);
		}

		var endTime = now();
		var elapsedSeconds = dateDiff("s", startTime, endTime);

		test.performance = {};
		test.performance.description = "Set " & nodes & " nodes";
		test.performance.elapsed = elapsedSeconds;


		debug(serializeJSON(test));

		db.kill("KBBMTEST", ["bigset"]);

		db.close();
	}

}