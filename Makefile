#
# CFMumps
#
# Makefile
#
# Author:   John P. Willis <jpw@coherent-logic.com>
# Modified: 21 Jan 2017
#
# Copyright (C) 2014, 2017 Coherent Logic Development LLC
#
# This file is part of CFMumps.
#
# CFMumps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFMumps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
#

DIST_DIRS = admin examples java lib mumps tests testbox xinetd
DIST_FILES = RELEASE_NOTES.txt README

VERSION := $(shell git describe --tags --long --always | \
	sed 's/v\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)-\?.*-\([0-9]*\)-\(.*\)/\1 \2 \3 \4 \5/g')
VERSION_MAJOR := $(word 1, $(VERSION))
VERSION_STRING := $(VERSION_MAJOR)

DIST_FILE_BASE = cfmumps-$(VERSION_STRING).tar


.PHONY: all java clean

all: java

dist: all README
	@echo Building distribution archive for CFMumps $(VERSION_STRING)...
	@mkdir build
	@for file in $(DIST_FILES); do (cp $$file build/); done
	@for dir in $(DIST_DIRS); do (cp -r $$dir build/); done
	@cd build; tar cf $(DIST_FILE_BASE) *; gzip $(DIST_FILE_BASE)
	@rm -f README

README: README.md
	markdown README.md | html2text -style pretty > README

java: 
	cd java; make cfgtm

clean:
	cd java; make clean
	@rm -f README
	@rm -rf build
	@rm -f java/cfmumps.jar

