<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>CFMumps</title>

		<link rel="stylesheet" href="admin/css/style.css">
	</head>
	<body>
		<cfset db = createObject("component", "lib.cfmumps.mumps")>
		<cfset db.open()>
		<cfset mVersion = db.mVersion()>
		<cfset db.close()>

		<div id="wrapper">
			<header>
				<ul>
					<li><a href="/admin/" target="_blank">Administration</a></li>
					<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/CFMumps+Home" target="_blank">Documentation</a></li>
				</ul>
			</header>
			<div id="content">
				<div class="content-wrap">
					<h1>Welcome to CFMumps</h1>
					<p>If you are seeing this page, you have installed CFMumps successfully!</p>

					<h2>Local Resources</h2>
					<ul>
						<li><a href="/admin/">Administration</a></li>
					</ul>

					<h2>Documentation</h2>
					<ul>
						<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/Developer+Guide" target="_blank">Developer Guide</a></li>
						<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/Developer+Reference" target="_blank">Developer Reference</a></li>
						<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/Administration" target="_blank">Administrator Guide</a></li>
						<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/Installation+Guide" target="_blank">Installation Guide</a></li>
					</ul>

					<h2>Community</h2>
					<ul>
						<li><a href="http://www.coherent-logic.com/cm/products/cfmumps" target="_blank">Home Page</a></li>
						<li><a href="https://groups.google.com/forum/#!forum/cfmumps-community" target="_blank">Forum</a></li>
						<li><a href="http://webchat.freenode.net/?channels=cfmumps" target="_blank">IRC Channel</a></li>
						<li><a href="https://cfmumps.atlassian.net/wiki/pages/viewrecentblogposts.action?key=CFM" target="_blank">Official Blog</a></li>
					</ul>
				</div>
			</div>
			<footer>
				<a href="http://www.coherent-logic.com/">Coherent Logic Development</a> | Running on <cfoutput>#mVersion#</cfoutput>
				<hr>
				Copyright &copy; 2014 Coherent Logic Development LLC
			</footer>
		</div>

	</body>
</html>