/*
 * CFMumps
 *
 * com.coherentlogic.cfmumps.intersystems.cache: Cache Extreme Connector glue
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 26 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
package com.coherentlogic.cfmumps.intersystems;
import com.intersys.globals.*;

public class Cache {
	
	private Connection connection = null;
	private int debugLevel = 4;
	
	public static final int MSG_ERROR = 1;
	public static final int MSG_WARNING = 2;
	public static final int MSG_INFO = 3;
	public static final int MSG_DEBUG = 4;
	
	public Cache() 
	{
		this.logMessage("CFMumps Cache Extreme Connector (Thread ID = " + Thread.currentThread().getId() + ")", this.MSG_INFO);
		this.logMessage("  Copyright (C) 2014, 2017 Coherent Logic Development LLC", this.MSG_INFO);

		this.logMessage("Entering Cache()", this.MSG_DEBUG);
		try {
			this.connection = null;
			this.logMessage("Calling ConnectionContext.getConnection()", this.MSG_DEBUG);
			this.connection = ConnectionContext.getConnection();
			this.logMessage("Finished ConnectionContext.getConnection()", this.MSG_DEBUG);
		}
		catch (Exception e) {
			this.logMessage(e.getMessage(), this.MSG_ERROR);
		}
		this.logMessage("Leaving Cache()", this.MSG_DEBUG);
	}
	
	private void logMessage(String message, int msgLevel)
	{
		String messageType = "";
		
		switch(msgLevel) {
		case 1:
			messageType = "Error";
			break;
		case 2:
			messageType = "Warning";
			break;
		case 3:
			messageType = "Information";
			break;
		case 4:
			messageType = "Debug";
			break;
		}
		
		if(msgLevel <= this.debugLevel) {
			System.out.println("CFMumps " + messageType + ":  " + message);
		}
	}
	
	public synchronized boolean open(String namespace, String user, String password, int debug)
	{
		this.debugLevel = debug;
	
		this.logMessage("Entering open()", this.MSG_DEBUG);
		this.logMessage(namespace, this.MSG_DEBUG);
		this.logMessage(user, this.MSG_DEBUG);
		this.logMessage(password, this.MSG_DEBUG);
		if(!this.isOpen()) {
			try {						
					this.connection.connect(namespace, user, password);
					this.logMessage("Leaving open() (this.connection was closed)", this.MSG_DEBUG);
					return(true);
				}
				catch(GlobalsException e) {
					this.logMessage(e.getMessage(), this.MSG_ERROR);
			
					return(false);
				}
		}
		else {
			this.logMessage("Leaving open() (this.connection was already open)", this.MSG_DEBUG);
			return(true);
		}
	}
	
	public synchronized boolean close()
	{
		this.logMessage("Entering close()", this.MSG_DEBUG);
		if(this.isOpen()) {
			try {
				this.connection.close();
				this.logMessage("Leaving close() (this.connection was open)", this.MSG_DEBUG);
				return(true);
			}
			catch (GlobalsException e) {
				this.logMessage(e.getMessage(), this.MSG_ERROR);
				return(false);
			}
		}
		else {
			this.logMessage("Leaving close() (this.connection was already closed)", this.MSG_DEBUG);
			return(true);
		}
	}
	
    public synchronized int set(String globalRef, String value) throws Exception 
    {
    	this.logMessage("Entering set()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("SET", "KBBMCIDT", globalRef, value);
    	
    	this.logMessage("Leaving set()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized String get(String globalRef) throws Exception 
    {
    	this.logMessage("Entering get()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("GET", "KBBMCIDT", globalRef).toString();
    	
    	this.logMessage("Leaving get()", this.MSG_DEBUG);
    	return(retVal);
    }

    public synchronized int kill(String globalRef) throws Exception 
    {
    	this.logMessage("Entering kill()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("KILL", "KBBMCIDT", globalRef);
    	
    	this.logMessage("Leaving kill()", this.MSG_DEBUG);
    	return(retVal);
    }

    public synchronized int data(String globalRef) throws Exception 
    {
    	this.logMessage("Entering data()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("DATA", "KBBMCIDT", globalRef);
    	
    	this.logMessage("Leaving data()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized String order(String globalRef) throws Exception 
    {
    	this.logMessage("Entering order()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("ORDER", "KBBMCIDT", globalRef).toString();
    	
    	this.logMessage("Leaving order()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized String query(String globalRef) throws Exception 
    {
    	this.logMessage("Entering query()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("QUERY", "KBBMCIDT", globalRef).toString();
    	
    	this.logMessage("Leaving query()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized int merge(String inputGlobalRef, String outputGlobalRef) throws Exception 
    {
    	this.logMessage("Entering merge()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("MERGE", "KBBMCIDT", inputGlobalRef, outputGlobalRef);
    	
    	this.logMessage("Leaving merge()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized int lock(String globalRef, int timeout) throws Exception 
    {
    	this.logMessage("Entering lock()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("LOCK", "KBBMCIDT", globalRef, timeout);
    	
    	this.logMessage("Leaving lock()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized int unlock(String globalRef) throws Exception 
    {
    	this.logMessage("Entering unlock()", this.MSG_DEBUG);
    	
    	int retVal = (Integer) this.connection.callFunction("UNLOCK", "KBBMCIDT", globalRef);
    	
    	this.logMessage("Leaving unlock()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized String version() throws Exception 
    {
    	this.logMessage("Entering version()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("VERSION", "KBBMCIDT").toString();

    	this.logMessage("Leaving version()", this.MSG_DEBUG);
    	
    	return(retVal);
    }

    public synchronized String function(String fn) throws Exception 
    {
    	this.logMessage("Entering function()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("FUNCTION", "KBBMCIDT", fn).toString();
    	
    	this.logMessage("Leaving function()", this.MSG_DEBUG);
    	
    	return(retVal);
    }    
    
    public synchronized boolean procedure(String procedure) throws Exception 
    {
    	this.logMessage("Entering procedure()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("PROCEDURE", "KBBMCIDT", procedure).toString();
    	
    	this.logMessage("Leaving procedure()", this.MSG_DEBUG);
    	
    	return(true);
    }  

	public synchronized String pid() throws Exception
	{
		this.logMessage("Entering pid()", this.MSG_DEBUG);
    	
    	String retVal = this.connection.callFunction("GETPID", "KBBMCIDT").toString();
    	
    	this.logMessage("Leaving pid()", this.MSG_DEBUG);
    	
    	return(retVal);
	}

    public synchronized boolean isOpen() 
    {
    	this.logMessage("Entering isOpen()", this.MSG_DEBUG);
    	
    	boolean retVal = this.connection.isConnected();
    	
    	this.logMessage("Leaving isOpen()", this.MSG_DEBUG);
    	
    	return(retVal);
    }
    
	public static void main(String args[]) {
		Cache cache = new Cache();
		
		cache.logMessage("Beginning tests", cache.MSG_DEBUG);
		
		boolean result = cache.open("USER", "_SYSTEM", "SYS", 4);
	
		if(!result) {
			cache.logMessage("Testing failed cache.open()", cache.MSG_ERROR);
			System.exit(1);
		}
		
		String glRef;
		
		for (int i = 1; i < 5000; i++) {
			glRef = new String("^jpw(" + Integer.toString(i) + ")");
			cache.logMessage("Setting " + glRef + "=1", cache.MSG_DEBUG);
			try {
				cache.set(glRef, "1");
			}
			catch(Exception ex) {
				cache.logMessage("Testing failed cache.set()", cache.MSG_ERROR);			
			}
		}
		
		System.exit(0);
	}
        	
}