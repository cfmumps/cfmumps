/*
 * CFMumps
 *
 * com.coherentlogic.cfmumps.fidelity.gtm:		GTMJI Connector glue
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 26 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/
package com.coherentlogic.cfmumps.fidelity;

import com.fis.gtm.ji.GTMCI;
import com.fis.gtm.ji.GTMLong;
import com.fis.gtm.ji.GTMString;
import com.fis.gtm.ji.GTMInteger;

public class Gtm {
	
	private int debugLevel = 2;
	
	private static final int MSG_ERROR = 1;
	private static final int MSG_WARNING = 2;
	private static final int MSG_INFO = 3;
	private static final int MSG_DEBUG = 4;
	
	public Gtm() {
		this.logMessage("CFMumps GTMJI Connector (Thread ID = " + Thread.currentThread().getId() + ")", this.MSG_INFO);
		this.logMessage("  Copyright (C) 2014, 2018 Coherent Logic Development LLC", this.MSG_INFO);
	}

	private void logMessage(String message, int msgLevel)
	{
		String messageType = "";
		
		switch(msgLevel) {
		case 1:
			messageType = "Error";
			break;
		case 2:
			messageType = "Warning";
			break;
		case 3:
			messageType = "Information";
			break;
		case 4:
			messageType = "Debug";
			break;
		}
		
		if(msgLevel <= this.debugLevel) {
			System.out.println("CFMumps " + messageType + ":  " + message);
		}
	}
	
    public int set(String globalRef, String value) throws Exception {
    	return(GTMCI.doIntJob("cfmumpsSet", globalRef, value));
    }

    public String get(String globalRef) throws Exception {
    	return(GTMCI.doStringJob("cfmumpsGet", globalRef));
    }

    public int increment(String globalRef, int count) throws Exception {
        GTMInteger countVal = new GTMInteger(count);
        return(GTMCI.doIntJob("cfmumpsIncrement", globalRef, countVal));
    }

    public int kill(String globalRef) throws Exception {
    	return(GTMCI.doIntJob("cfmumpsKill", globalRef));
    }

    public int data(String globalRef) throws Exception {
    	return(GTMCI.doIntJob("cfmumpsData", globalRef));	   
    }

    public String order(String globalRef) throws Exception {
    	return(GTMCI.doStringJob("cfmumpsOrder", globalRef));
    }

    public String query(String globalRef) throws Exception {
    	return(GTMCI.doStringJob("cfmumpsQuery", globalRef));
    }

    public int merge(String inputGlobalRef, String outputGlobalRef) throws Exception {
    	return(GTMCI.doIntJob("cfmumpsMerge", inputGlobalRef, outputGlobalRef));
    }

    public int lock(String globalRef, int timeout) throws Exception {
    	GTMInteger timeoutVal = new GTMInteger(timeout);
    	return(GTMCI.doIntJob("cfmumpsLock", globalRef, timeoutVal));
    }

    public int unlock(String globalRef) throws Exception {
    	return(GTMCI.doIntJob("cfmumpsUnlock", globalRef));
    }

    public String version() throws Exception {
    	return(GTMCI.doStringJob("cfmumpsVersion"));       
    }

    public String function(String fn, int autoRelink) throws Exception {
    	GTMInteger alink = new GTMInteger(autoRelink);
    	return(GTMCI.doStringJob("cfmumpsFunction", fn, alink));
    }
    
    public String procedure(String procedure, int autoRelink) throws Exception {
		GTMInteger alink = new GTMInteger(autoRelink);
		return(GTMCI.doStringJob("cfmumpsProcedure", procedure, alink));
	}
    
    public String mEval(String fn, String outputFile, String routineName, String routinePath) throws Exception {
    	return(GTMCI.doStringJob("cfmumpsEval", fn, outputFile, routineName, routinePath));
    }
    
    public long pid() throws Exception {
    	return(GTMCI.doLongJob("cfmumpsPid"));
    }

    public String lastError() {
    	return("");
    }

    public boolean isOpen() {
    	return(true);
    }
    
}
