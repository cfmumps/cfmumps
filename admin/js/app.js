var cfmumps = {};

cfmumps.administrator = {
	
	onReady: function (tabIndex) {
		$("#tabs").tabs({
			active: tabIndex,
			activate: function(event, ui) {				
				var index = ui.newTab.index();
				var url = "/admin/setTab.cfm?tabIndex=" + escape(index);
				$.ajax(url);
			}
		});
	}
	
	
};
