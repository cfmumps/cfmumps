<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>CFMumps Administrator</title>

		<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/flick/jquery-ui.css">
		<link rel="stylesheet" href="css/style.css">

		<style type="text/css">
			select {
				margin: 0;
			}
		</style>
	</head>
	<body>
		<cfset db = new lib.cfmumps.Mumps()>
		<cfset db.open()>
		<cfset mumpsVersion = db.version()>
		<cfset db.close()>

		<cfset admin = new lib.cfmumps.Admin()>
		<cfset connectors = listToArray(admin.getConfig("General", "connectors"))>

		<div id="wrapper">
			<header>
				<ul>
					<li><a href="https://cfmumps.atlassian.net/wiki/display/CFM/CFMumps+Home" target="_blank">Documentation</a></li>
					<cfif session.loggedIn EQ true>
						<li><a href="logout.cfm">Logout</a></li>
					</cfif>
				</ul>
			</header>
			<cfif session.loggedIn EQ true>
				<div id="content">					
					<div class="content-wrap">

						<div id="tabs">
							<ul>
								<li><a href="#overview">Overview</a></li>
								<cfloop array="#connectors#" index="connector">
									<cfset obj = createObject("component", connector)>
									<cfset name = obj.connectorName>
									<cfset version = obj.connectorVersion>
									<cfoutput><li><a href="###connector#">#name#</a></li></cfoutput>
								</cfloop>								
							</ul>

							<div id="overview">
								<cfinclude template="overview.cfm">
							</div>

							<cfloop array="#connectors#" index="connector">
								<cfset obj = createObject("component", connector)>
								<cfset name = obj.connectorName>
								<cfset version = obj.connectorVersion>
								<cfoutput>
									<div id="#connector#">
										#obj.getSettings()#
									</div>
								</cfoutput>
							</cfloop>

						</div>

					</div>
				</div>
			<cfelse>
				<div id="content">
					<div id="content-wrap">
						<cfinclude template="login.cfm">
					</div>
				</div>

			</cfif>
			<footer>
				<a href="http://www.coherent-logic.com/">Coherent Logic Development</a> | Running on <cfoutput>#mumpsVersion#</cfoutput>
				<hr>
				Copyright &copy; 2014, 2018 Coherent Logic Development LLC
			</footer>
		</div>
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/admin/js/app.js"></script>

		<script type="text/javascript">
			$(document).ready(function () {
				<cfif isDefined("session.lastTabIndex")>
					<cfoutput>
						cfmumps.administrator.onReady(#session.lastTabIndex#);
					</cfoutput>
				<cfelse>
					cfmumps.administrator.onReady(0);
				</cfif>
			});
		</script>
	</body>
</html>