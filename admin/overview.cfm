<cfset admin = new lib.cfmumps.Admin()>

<cfif isDefined("overview.submit")>
	<cfset section = "general">
	<cfset admin.setConfig(section, "connector", form.overview.defaultConnector)>
	<cfset admin.setConfig(section, "logLevel", form.overview.loggingLevel)>

	<div class="notification-box">
		<div>Changes saved</div>

		Your changes have been saved. Depending on the nature of the changes you performed,
		you may need to restart your application server in order for them to take effect.
	</div>
</cfif>

<cfset system = createObject("java", "java.lang.System")>
<cfset environment = system.getenv()>
<cfset classpath = environment.get("CLASSPATH")>
<cfset connectors = listToArray(admin.getConfig("General", "connectors"))>
<cfset defaultConnector = admin.getConfig("General", "connector")>
<cfset logLevel = admin.getConfig("General", "logLevel")>

<form method="post" action="default.cfm">
<div class="row">
	<div class="left-column">
		CFML Engine
	</div>
	<div class="right-column">
		<cfoutput>#server.coldfusion.productname# #server.coldfusion.productversion#</cfoutput>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Operating System
	</div>
	<div class="right-column">
		<cfoutput>#server.os.name# #server.os.version#</cfoutput>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Java Version
	</div>
	<div class="right-column">
		<cfoutput>#server.java.version# (#server.java.vendor#)</cfoutput>
	</div>
</div>

<!--- <div class="row">
	<div class="left-column">
		Java Class Path
	</div>
	<div class="right-column">
		<cfoutput>#classpath#</cfoutput>
	</div>
</div> --->

<div class="row">
	<div class="left-column">
		MUMPS Version
	</div>
	<div class="right-column">
		<cfset db = new lib.cfmumps.Mumps()>
		<cfset db.open()>
		<cfset mVersion = db.version()>
		<cfset db.close()>
		<cfoutput>#mVersion#</cfoutput>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Default Connector
	</div>
	<div class="right-column">
		<select name="overview.defaultConnector">
		<cfloop array="#connectors#" index="connector">
			<cfset obj = createObject("component", connector)>
			<cfset name = obj.connectorName>
			<cfset version = obj.connectorVersion>
			<cfoutput>
				<option value="#connector#" <cfif connector EQ defaultConnector>selected="selected"</cfif>>#name# #version#</option>
			</cfoutput>
		</cfloop>
		</select>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Logging Level
	</div>
	<div class="right-column">
		<select name="overview.loggingLevel">
			<option value="0" <cfif logLevel EQ 0>selected="selected"</cfif>>None</option>
			<option value="1" <cfif logLevel EQ 1>selected="selected"</cfif>>Error</option>
			<option value="2" <cfif logLevel EQ 2>selected="selected"</cfif>>Warning</option>
			<option value="3" <cfif logLevel EQ 3>selected="selected"</cfif>>Information</option>
			<option value="4" <cfif logLevel EQ 4>selected="selected"</cfif>>Debug</option>
		</select>
	</div>
</div>

<input type="submit" name="overview.submit" value="Save Changes">
</form>


