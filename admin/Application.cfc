<cfcomponent displayName="Application" output="true">

	<cfset this.Name = "cfmumpsAdministrator">
	<cfset this.ApplicationTimeout = CreateTimeSpan(0, 1, 0, 0)>
	<cfset this.SessionTimeout = CreateTimeSpan(0, 1, 0, 0)>
	<cfset this.SessionManagement = true>
	<cfset this.SetClientCookies = true>

	<cfsetting requesttimeout="500" showdebugoutput="false" enablecfoutputonly="false">

	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="true">


		<cfreturn true>
	</cffunction>

	<cffunction name="OnSessionStart" access="public" returntype="boolean" output="true">
		<cfset session.loggedIn = false>

		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="true">


		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequest" access="public" returntype="void" output="true">
		<cfargument name="TargetPage" type="string" required="true">

		<cfinclude template="#arguments.TargetPage#">

		<cfreturn>
	</cffunction>

	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true">

		<cfreturn>
	</cffunction>

	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false">
		<cfargument name="SessionScope" type="struct" required="true">
		<cfargument name="ApplicationScope" type="struct" required="true">

		<cfreturn>
	</cffunction>

	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false">
		<cfargument name="ApplicationScope" type="struct" required="false" default="#structNew()#">

		<cfreturn>
	</cffunction>

</cfcomponent>
