<cfset admin = createObject("component", "lib.cfmumps.admin")>
<cfset passwordHash = admin.getConfig("administrator", "password")>
<cfif passwordHash EQ "">
	<cfset mode = "createPassword">
	<cfset loginMessage = "Please create your password">
<cfelse>
	<cfset mode = "login">
	<cfset loginMessage = "">
</cfif>



<cfif isDefined("form.password")>
	<cfswitch expression="#mode#">
		<cfcase value="createPassword">
			<cfset admin.setConfig("administrator", "password", hash(form.password))>
			<cfset session.loggedIn = true>
			<cflocation url="default.cfm" addtoken="false">
		</cfcase>
		<cfcase value="login">
			<cfif hash(form.password) EQ passwordHash>
				<cfset session.loggedIn = "true">
				<cflocation url="default.cfm" addtoken="false">
			<cfelse>
				<cfset loginMessage = "Invalid password.">
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>

<cfoutput>
	<div class="login-panel">
		<h1 style="text-align:center;">CFMumps Administrator</h1>
		<hr>
		<div style="font-weight: bold; color: blue; text-align: center;">#loginMessage#</div>
		<form name="login" action="default.cfm" method="post">
			<fieldset>
				<label for="password">Password</label>
				<input type="password" name="password" id="password" placeholder="Please enter your password">
			</fieldset>
			<div style="text-align: right; width:120px; float: right; margin-top: 20px;">
			<input type="submit" name="submit" value="Log In">
			</div>
		</form>
	</div>
</cfoutput>