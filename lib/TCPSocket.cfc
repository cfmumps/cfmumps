/*
 * TCPSocket
 *
 * lib.tcpSocket:		CFScript TCP Socket Wrapper
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 16 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=false displayName=tcpSocket {

	this.host = "";
	this.port = "";
	this.socket = "";
	this.success = false;
	this.errorMessage = "";
	this.response = "";
	this.logLevel = 4;

	public component function open(required string host, required numeric port) output=false
	{
		this.host = arguments.host;
		this.port = arguments.port;
		this.socket = createObject("java", "java.net.Socket");

		try {
			this.socket.init(this.host, this.port);
		}
		catch(any exception) {
			this.success = false;
			this.errorMessage = "tcpSocket.open(): could not connect to " & this.host & ":" & this.port;

			return this;
		}

		this.outputStream = this.socket.getOutputStream();
		this.output = createObject("java", "java.io.PrintWriter").init(this.outputStream);
		this.inputStream = this.socket.getInputStream();
		this.inputStreamReader = createObject("java", "java.io.InputStreamReader").init(this.inputStream);
		this.input = createObject("java", "java.io.BufferedReader").init(this.inputStreamReader);

		this.success = true;
		this.errorMessage = "";

		return this;
	}

	public component function close() output=false
	{
		if(this.socket.isConnected()) {
			this.success = true;
			this.errorMessage = "";
			this.socket.close();
		}
		else {
			this.success = false;
			this.errorMessage = "tcpSocket.close(): socket not open";
		}

		return this;
	}

	public component function send(required string message) output=false
	{
		if(this.logLevel >= 3) {
			writeLog(text="send: " & message, file="tcpSocket", type="Information");
		}

		if(this.socket.isConnected()) {
			this.output.println(arguments.message);
			this.output.println();
			this.output.flush();

			this.response = "";
			this.success = true;
			this.errorMessage = "";
		}
		else {
			this.success = false;
			this.errorMessage = "tcpSocket.send(): socket not connected";
		}

		return this;
	}

	public component function read() output=false
	{
		if(this.socket.isConnected()) {
			this.response = this.input.readLine();

			if(this.logLevel >= 3) {
				writeLog(text="read: " & this.response, file="tcpSocket", type="Information");
			}

			this.success = true;
			this.errorMessage = "";
		}
		else {
			this.success = false;
			this.errorMessage = "tcpSocket.read(): socket not connected";
		}

		return this;
	}

}
