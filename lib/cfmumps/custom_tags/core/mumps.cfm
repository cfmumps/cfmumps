<cfif NOT isDefined("thisTag")>
	<cfthrow message="This must be called as a tag">
</cfif>

<cfif NOT thisTag.hasEndTag>
	<cfthrow message="This tag requires both an opening and closing tag">
</cfif>

<cfif thisTag.executionMode is "end">
	<cfset code = thisTag.GeneratedContent>
	<cfset lf = chr(10)>
	<cfset crLf = chr(13) & chr(10)>
	<cfset codeArray = listToArray(code, lf, false, true)>
	<cfset code = arrayToList(codeArray, lf)>
	<cftry>
		<cfset fileName = createUUID() & ".cfmumps">


		<cfset db = createObject("lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfset routineName = "CFM" & db.getPiece(createUUID(), "-", 1)>

		<cfset results = db.mEval(code, fileName, routineName)>

		<cfset db.close()>

		<cfset results = replace(results, lf, "<br>", "all")>

		<cfset thisTag.generatedContent = results>

		<cfcatch>
			<cfdump var="#cfcatch#">
			<cfset thisTag.generatedContent = "Error">
		</cfcatch>
	</cftry>
</cfif>
