/*
 * CFMumps
 *
 * lib.cfmumps.connector.GTMJI:		CFMumps connector for GT.M (GTMJI)
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 19 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=false implements=lib.cfmumps.IMumpsConnector extends=lib.cfmumps.Admin {

		this.connectorName = "GTMJI Connector";
		this.connectorVersion = "0.7.1";
		this.admin = createObject("component", "lib.cfmumps.Admin");
		this.autoRelink = false;
		this.dbOpen = false;
		this.logLevel = this.admin.getConfig("lib.cfmumps.connector.GTMJI", "logLevel");
		this.routinePath = this.admin.getConfig("lib.cfmumps.connector.GTMJI", "routineOutputPath");

		this.implementsNativeGetObject = false;
		this.implementsNativeSetObject = false;

		public void function getSettings() output=true
		{
			include "settings/GTMJI.cfm";
		}

		public void function saveSettings() output=true
		{
			include "settings/GTMJI.cfm";
		}

		public component function open(boolean autoRelink) output=false
		{
			var system = createObject("java", "java.lang.System");
			var environment = system.getenv();
			var relinkEnv = environment.get("CFMUMPS_AUTORELINK");

			if(isDefined("relinkEnv")) {
				if(lcase(relinkEnv) is "true" or relinkEnv is 1) {
					this.autoRelink = true;
				}
			}

			if(isDefined("arguments.autoRelink")) {
				this.autoRelink = arguments.autoRelink;
			}

			this.gtm = createObject("java", "com.coherentlogic.cfmumps.fidelity.Gtm");
			this.dbOpen = true;

			return this;
		}

		public boolean function isOpen() output=false
		{
			return this.dbOpen;
		}

		public component function close() output=false
		{
			this.dbOpen = false;

			return this;
		}

		public void function set(required string globalName, required array subscripts, required string value) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			this.gtm.set(glRef, arguments.value);
		}

		public any function get(required string globalName, required array subscripts) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			return this.gtm.get(glRef);
		}

		public numeric function increment(required string globalName, required array subscripts, required numeric count=1) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			return this.gtm.increment(glRef, count);
		}

		public void function kill(required string globalName, required array subscripts) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			this.gtm.kill(glRef);
		}

		public struct function data(required string globalName, required array subscripts) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			var defined = this.gtm.data(glRef);
			var result = {};

			switch(defined) {
				case "0":
					result.defined = false;
					result.hasData = false;
					result.hasSubscripts = false;
					break;
				case "1":
					result.defined = true;
					result.hasData = true;
					result.hasSubscripts = false;
					break;
				case "10":
					result.defined = true;
					result.hasData = false;
					result.hasSubscripts = true;
					break;
				case "11":
					result.defined = true;
					result.hasData = true;
					result.hasSubscripts = true;
					break;
			}

			return result;
		}

		public struct function order(required string globalName, required array subscripts) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, true);

			var ord = this.gtm.order(glRef);
			var result = {};

			if(ord is "") {
				result.lastResult = true;
				result.value = "";
			}
			else {
				result.lastResult = false;
				result.value = ord;
			}

			return result;
		}

		public string function query(required string globalRef) output=false
		{
			return this.gtm.query(arguments.globalRef);
		}

		public numeric function merge(required string inputGlobalName,
									  required array inputGlobalSubscripts,
									  required string outputGlobalName,
									  required array outputGlobalSubscripts) output=false
		{
			var inputGlRef = this.normalizeGlRef(arguments.inputGlobalName, arguments.inputGlobalSubscripts, false);
			var outputGlRef = this.normalizeGlRef(arguments.outputGlobalName, arguments.outputGlobalSubscripts, false);

			return this.gtm.merge(inputGlRef, outputGlRef);
		}

		public boolean function lock(required string globalName, required array subscripts, required numeric timeout=0) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			var status = this.gtm.lock(glRef, timeout);

			if(status is 1) {
				return true;
			}
			else {
				return false;
			}
		}

		public boolean function unlock(required string globalName, required array subscripts) output=false
		{
			var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

			try {
				var status = this.gtm.unlock(glRef);
				return true;
			}
			catch(any exception) {
				return false;
			}

		}

		public string function version() output=false
		{
			try {
				return this.gtm.version();
			}
			catch(any exception) {
				return "Connector error";
			}
		}

		public numeric function pid() output=false
		{
			return this.gtm.pid();
		}

		public string function get_horolog() output=false
        {
            return this.mumps_function("GETHOROLOG^KBBMCIDT", [], false);
        }

        public string function convert_horolog(required string horolog) output=false
        {
            return this.mumps_function("CVTHOROLOG^KBBMCIDT", [arguments.horolog], false);
        }

		public any function mumps_function(required string fn, required array args, boolean autoRelink) output=false
		{
			var alink = 0;

			if(isDefined("arguments.autoRelink")) {
				if(arguments.autoRelink is true) {
					alink = 1;
				}
			}

			if(this.autoRelink is true) {
				alink = 1;
			}

			return this.gtm.function(this.normalizeFunctionCall(arguments.fn, arguments.args), alink);
		}

		public boolean function mumps_procedure(required string procedure, required array args, boolean autoRelink) output=false
		{
			var alink = 0;

			if(isDefined("arguments.autoRelink")) {
				if(arguments.autoRelink is true) {
					alink = 1;
				}
			}

			if(this.autoRelink is true) {
				alink = 1;
			}

			try {
				this.gtm.procedure(this.normalizeFunctionCall(arguments.procedure, arguments.args), alink);
				return true;
			}
			catch(any exception) {
				return false;
			}
		}

		public string function mumps_eval(required string mCode, required string outputFile, required string routineName) output=false
		{
			try {
				var result = this.gtm.mEval(arguments.mCode, arguments.outputFile, arguments.routineName, this.routinePath);
				var fileName = this.routinePath & arguments.routineName & ".m";
			}
			catch (any exception) {
				return "";
			}

			try {
				fileDelete(fileName);
			}
			catch (any exception) {}

			return result;
		}

		public string function normalizeGlRef(required string globalName, required array subscripts, boolean allowEmptyLastSubscript=false) output=false
		{
			var subscriptCount = 0;

			switch(left(arguments.globalName, 1)) {
				case "^":	//global sigil 
					//pass the global name through as-is
					var retVal = arguments.globalName;
					break;
				case "$":	//local sigil
					//strip off the sigil, so that ^KBBMCIDT's indirection will treat this as a local variable
					var retVal = mid(arguments.globalName, 2);
					break;
				default:	//no recognized sigil
					//here, we assume it's a global, so prepend a caret
					var retVal = "^" & arguments.globalName;
					break;
			}

			if(subscripts.len() > 0) {
				retVal &= "(";

				for(sub in arguments.subscripts) {
					subscriptCount++;

					if(isNumeric(sub)) {
						retVal &= sub & ",";
					}
					else {
						if(sub is "") {
							if(subscriptCount != subscripts.len()) {
								throw(message = "Subscripts must not be empty.");
							}
							else {
								if(not arguments.allowEmptyLastSubscript) {
									throw(message = "Subscripts must not be empty.");
								}
							}
						}
						retVal &= chr(34) & sub & chr(34) & ",";
					}
				}
				retVal = left(retVal, len(retVal) - 1) & ")";
			}

			return retVal;
		}

		public string function normalizeFunctionCall(required string functionName, required array functionArgs) output=false
		{
			var retVal = functionName;

			if(functionArgs.len() > 0) {
				retVal &= "(";

				for(arg in arguments.functionArgs) {
					if(isNumeric(arg)) {
						retVal &= arg & ",";
					}
					else {
						retval &= chr(34) & arg & chr(34) & ",";
					}
				}
				retVal = left(retVal, len(retVal) - 1) & ")";
			}

			return retVal;
		}

}