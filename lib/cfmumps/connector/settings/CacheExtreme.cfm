<cfset admin = new lib.cfmumps.Admin()>

<cfif isDefined("form.CacheExtreme.submit")>
	<cfset section = "lib.cfmumps.connector.CacheExtreme">

	<cfset admin.setConfig(section, "logLevel", form.CacheExtreme.loggingLevel)>
	<cfset admin.setConfig(section, "namespace", form.CacheExtreme.namespace)>
	<cfset admin.setConfig(section, "user", form.CacheExtreme.user)>
	<cfset admin.setConfig(section, "password", form.CacheExtreme.password)>

	<div class="notification-box">
		<div>Changes saved</div>

		Your changes have been saved. Depending on the nature of the changes you performed,
		you may need to restart your application server in order for them to take effect.
	</div>
</cfif>



<cfset logLevel = admin.getConfig("lib.cfmumps.connector.CacheExtreme", "logLevel")>
<cfset namespace = admin.getConfig("lib.cfmumps.connector.CacheExtreme", "namespace")>
<cfset user = admin.getConfig("lib.cfmumps.connector.CacheExtreme", "user")>
<cfset password = admin.getConfig("lib.cfmumps.connector.CacheExtreme", "password")>

<cfset obj = createObject("component", "lib.cfmumps.connector.CacheExtreme")>


<form method="post" action="default.cfm">
<cfoutput>
<div class="row">
	<div class="left-column">
		Connector Name
	</div>
	<div class="right-column">
		#obj.connectorName#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Connector Version
	</div>
	<div class="right-column">
		#obj.connectorVersion#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Logging Level
	</div>
	<div class="right-column">
		<select name="CacheExtreme.loggingLevel">
			<option value="0" <cfif logLevel EQ 0>selected="selected"</cfif>>None</option>
			<option value="1" <cfif logLevel EQ 1>selected="selected"</cfif>>Error</option>
			<option value="2" <cfif logLevel EQ 2>selected="selected"</cfif>>Warning</option>
			<option value="3" <cfif logLevel EQ 3>selected="selected"</cfif>>Information</option>
			<option value="4" <cfif logLevel EQ 4>selected="selected"</cfif>>Debug</option>
		</select>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Namespace
	</div>
	<div class="right-column">
		<input type="text" name="CacheExtreme.namespace" value="#namespace#">
	</div>
</div>

<div class="row">
	<div class="left-column">
		User
	</div>
	<div class="right-column">
		<input type="text" name="CacheExtreme.user" value="#user#">
	</div>
</div>

<div class="row">
	<div class="left-column">
		Password
	</div>
	<div class="right-column">
		<input type="password" name="CacheExtreme.password" value="#password#">
	</div>
</div>


</cfoutput>
<input type="submit" name="CacheExtreme.submit" value="Save Changes">

<div class="notification-box">
	<div>Prerelease Software Warning</div>

	<p>Please note that this connector is an alpha prerelease for development evaluation only. Although it will often pass the unit tests contained in the
	CFMumps test suite, it is prone to segmentation faults, which will crash the CF application server along with CFMumps itself.</p>

	<p>Coherent Logic Development LLC cannot be held liable for data loss or service interruptions caused by the use of the Cache Connector.</p>
</div>
</form>
