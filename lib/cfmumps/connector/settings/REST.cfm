<cfset admin = new lib.cfmumps.Admin()>
<cfset section = "lib.cfmumps.connector.REST">

<cfif isDefined("form.REST.submit")>
   
    <cfset admin.setConfig(section, "server", form.REST.server)>
    <cfset admin.setConfig(section, "port", form.REST.port)>

    <div class="notification-box">
        <div>Changes saved</div>

        Your changes have been saved. With the REST connector,
        you will not need to restart your application server in order for them to take effect.
    </div>
</cfif>


<cfset obj = createObject("component", "lib.cfmumps.connector.REST")>
<cfset restServer = admin.getConfig(section, "server")>
<cfset restPort = admin.getConfig(section, "port")>

<form method="post" action="default.cfm">
<cfoutput>
<div class="row">
    <div class="left-column">
        Connector Name
    </div>
    <div class="right-column">
        #obj.connectorName#
    </div>
</div>

<div class="row">
    <div class="left-column">
        Connector Version
    </div>
    <div class="right-column">
        #obj.connectorVersion#
    </div>
</div>


<div class="row">
    <div class="left-column">
        Server
    </div>
    <div class="right-column">        
        <input type="text" name="REST.server" value="#restServer#">
    </div>
</div>

<div class="row">
    <div class="left-column">
        Port
    </div>
    <div class="right-column">
        <input type="text" name="REST.port" value="#restPort#">
    </div>
</div>

</cfoutput>
<input type="submit" name="REST.submit" value="Save Changes">
</form>
