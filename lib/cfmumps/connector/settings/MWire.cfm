<cfset admin = new lib.cfmumps.Admin()>

<cfif isDefined("form.MWire.submit")>
	<cfset section = "lib.cfmumps.connector.MWire">
	<cfset admin.setConfig(section, "host", form.MWire.host)>
	<cfset admin.setConfig(section, "port", form.MWire.port)>
	<cfset admin.setConfig(section, "password", form.MWire.password)>
	<cfset admin.setConfig(section, "logLevel", form.MWire.loggingLevel)>

	<cfif isDefined("form.MWire.secure")>
		<cfset admin.setConfig(section, "secure", "true")>
	<cfelse>
		<cfset admin.setConfig(section, "secure", "false")>
	</cfif>

	<div class="notification-box">
		<div>Changes saved</div>

		Your changes have been saved. Depending on the nature of the changes you performed,
		you may need to restart your application server in order for them to take effect.
	</div>
</cfif>

<cfset logLevel = admin.getConfig("lib.cfmumps.connector.MWire", "logLevel")>
<cfset host = admin.getConfig("lib.cfmumps.connector.MWire", "host")>
<cfset port = admin.getConfig("lib.cfmumps.connector.MWire", "port")>
<cfset secure = admin.getConfig("lib.cfmumps.connector.MWire", "secure")>
<cfset password = admin.getConfig("lib.cfmumps.connector.MWire", "password")>
<cfset obj = createObject("component", "lib.cfmumps.connector.MWire")>

<form method="post" action="default.cfm">
<cfoutput>
<div class="row">
	<div class="left-column">
		Connector Name
	</div>
	<div class="right-column">
		#obj.connectorName#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Connector Version
	</div>
	<div class="right-column">
		#obj.connectorVersion#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Logging Level
	</div>
	<div class="right-column">
		<select name="MWire.loggingLevel">
			<option value="0" <cfif logLevel EQ 0>selected="selected"</cfif>>None</option>
			<option value="1" <cfif logLevel EQ 1>selected="selected"</cfif>>Error</option>
			<option value="2" <cfif logLevel EQ 2>selected="selected"</cfif>>Warning</option>
			<option value="3" <cfif logLevel EQ 3>selected="selected"</cfif>>Information</option>
			<option value="4" <cfif logLevel EQ 4>selected="selected"</cfif>>Debug</option>
		</select>
	</div>
</div>

<div class="row">
	<div class="left-column">
		M/Wire Host
	</div>
	<div class="right-column">
		<input type="text" name="MWire.host" value="#host#">
	</div>
</div>

<div class="row">
	<div class="left-column">
		M/Wire Port
	</div>
	<div class="right-column">
		<input type="text" name="MWire.port" value="#port#">
	</div>
</div>


<div class="row">
	<div class="left-column">
		Security
	</div>
	<div class="right-column">
		<input type="checkbox" name="MWire.secure" id="MWire.secure" <cfif secure EQ "true">checked="checked"</cfif>>
		<label for="MWire.secure">Enabled</label>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Password
	</div>
	<div class="right-column">
		<input type="password" name="MWire.password" value="#password#">
	</div>
</div>
</cfoutput>
<input type="submit" name="MWire.submit" value="Save Changes">
</form>