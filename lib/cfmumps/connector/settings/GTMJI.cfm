<cfset admin = new lib.cfmumps.Admin()>

<cfif isDefined("form.GTMJI.submit")>
	<cfset section = "lib.cfmumps.connector.GTMJI">

	<cfif right(form.GTMJI.routineOutputPath, 1) NEQ "/">
		<cfset form.GTMJI.routineOutputPath &= "/">
	</cfif>

	<cfset admin.setConfig(section, "routineOutputPath", form.GTMJI.routineOutputPath)>
	<cfset admin.setConfig(section, "logLevel", form.GTMJI.loggingLevel)>

	<div class="notification-box">
		<div>Changes saved</div>

		Your changes have been saved. Depending on the nature of the changes you performed,
		you may need to restart your application server in order for them to take effect.
	</div>
</cfif>



<cfset logLevel = admin.getConfig("lib.cfmumps.connector.GTMJI", "logLevel")>
<cfset routineOutputPath = admin.getConfig("lib.cfmumps.connector.GTMJI", "routineOutputPath")>
<cfset obj = createObject("component", "lib.cfmumps.connector.GTMJI")>

<cfset system = createObject("java", "java.lang.System")>
<cfset environment = system.getenv()>
<cfset gtmroutines = environment.get("gtmroutines")>
<cfset gtm_dist = environment.get("gtm_dist")>
<cfset gtmgbldir = environment.get("gtmgbldir")>

<cftry>
	<cfset filename = routineOutputPath & createUUID() & ".tmp">
	<cffile action="write" file="#filename#" output="test">
	<cffile action="delete" file="#filename#">

	<cfset outputPathOK = true>

	<cfcatch>
		<cfset outputPathOK = false>
	</cfcatch>
</cftry>

<form method="post" action="default.cfm">
<cfoutput>
<div class="row">
	<div class="left-column">
		Connector Name
	</div>
	<div class="right-column">
		#obj.connectorName#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Connector Version
	</div>
	<div class="right-column">
		#obj.connectorVersion#
	</div>
</div>

<div class="row">
	<div class="left-column">
		Routine Search Path
	</div>
	<div class="right-column">
		<cfif isDefined("gtmroutines")>
			#gtmroutines#
		</cfif>
	</div>
</div>

<div class="row">
	<div class="left-column">
		GT.M Location
	</div>
	<div class="right-column">
		<cfif isdefined("gtm_dist")>
			#gtm_dist#
		</cfif>
	</div>
</div>

<div class="row">
	<div class="left-column">
		GT.M Global Directory
	</div>
	<div class="right-column">
		<cfif isdefined("gtmgbldir")>
			#gtmgbldir#
		</cfif>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Logging Level
	</div>
	<div class="right-column">
		<select name="GTMJI.loggingLevel">
			<option value="0" <cfif logLevel EQ 0>selected="selected"</cfif>>None</option>
			<option value="1" <cfif logLevel EQ 1>selected="selected"</cfif>>Error</option>
			<option value="2" <cfif logLevel EQ 2>selected="selected"</cfif>>Warning</option>
			<option value="3" <cfif logLevel EQ 3>selected="selected"</cfif>>Information</option>
			<option value="4" <cfif logLevel EQ 4>selected="selected"</cfif>>Debug</option>
		</select>
	</div>
</div>

<div class="row">
	<div class="left-column">
		Routine Output Path
	</div>
	<div class="right-column">
		<input type="text" name="GTMJI.routineOutputPath" value="#routineOutputPath#">
		<cfif outputPathOK EQ false>
			<br>
			<div style="color: red; font-weight: bold;">Cannot write to this path. mEval() API and the &lt;cf_mumps&gt; custom tag will not work.</div>
		</cfif>
	</div>
</div>
</cfoutput>
<input type="submit" name="GTMJI.submit" value="Save Changes">
</form>
