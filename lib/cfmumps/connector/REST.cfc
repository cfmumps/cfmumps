/*
 * CFMumps
 *
 * lib.cfmumps.connector.REST:     CFMumps connector for REST
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 19 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=false implements=lib.cfmumps.IMumpsConnector extends=lib.cfmumps.Admin {

        this.connectorName = "REST Connector";
        this.connectorVersion = "0.1.1";
        this.admin = createObject("component", "lib.cfmumps.Admin");
        this.autoRelink = false;
        this.dbOpen = false;        
        this.server = this.admin.getConfig("lib.cfmumps.connector.REST", "server");
        this.port = this.admin.getConfig("lib.cfmumps.connector.REST", "port");

        this.implementsNativeGetObject = true;
        this.implementsNativeSetObject = true;
        
        public void function getSettings() output=true
        {
            include "settings/REST.cfm";
        }

        public void function saveSettings() output=true
        {
            include "settings/REST.cfm";
        }

        public component function open(boolean autoRelink) output=false
        {
            var system = createObject("java", "java.lang.System");
            var environment = system.getenv();
            var relinkEnv = environment.get("CFMUMPS_AUTORELINK");

            if(isDefined("relinkEnv")) {
                if(lcase(relinkEnv) is "true" or relinkEnv is 1) {
                    this.autoRelink = true;
                }
            }

            if(isDefined("arguments.autoRelink")) {
                this.autoRelink = arguments.autoRelink;
            }
            
            this.dbOpen = true;

            return this;
        }

        public boolean function isOpen() output=false
        {
            return this.dbOpen;
        }

        public component function close() output=false
        {
            this.dbOpen = false;

            return this;
        }

        public struct function call_rest(required string endpoint, required struct postBody)
        {
            var turl = "http://" & this.server & ":" & this.port & "/" & arguments.endpoint;            

            http url=turl method="post" result="result" {
                httpparam type="header" name="Content-Type" value="application/json";
                httpparam type="body" value="#serializeJson(arguments.postBody)#";
            }

            var output = deserializeJSON(result.filecontent);

            if(!output.success) {
                throw(output.message);
            }

            return output;
        }

        public void function set(required string globalName, required array subscripts, required string value) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts,
                value: arguments.value
            };

            this.call_rest("set", postBody);
        }

        public any function get(required string globalName, required array subscripts) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };

            return this.call_rest("get", postBody).value;
        }

        public numeric function increment(required string globalName, required array subscripts, required numeric count=1) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts,
                count: arguments.count
            };

            return this.call_rest("increment", postBody).value;
        }

        public void function kill(required string globalName, required array subscripts) output=false
        {

            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };

            this.call_rest("kill", postBody);

        }

        public struct function data(required string globalName, required array subscripts) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };

            var defined = this.call_rest("data", postBody).value;
            var result = {};

            switch(defined) {
                case "0":
                    result.defined = false;
                    result.hasData = false;
                    result.hasSubscripts = false;
                    break;
                case "1":
                    result.defined = true;
                    result.hasData = true;
                    result.hasSubscripts = false;
                    break;
                case "10":
                    result.defined = true;
                    result.hasData = false;
                    result.hasSubscripts = true;
                    break;
                case "11":
                    result.defined = true;
                    result.hasData = true;
                    result.hasSubscripts = true;
                    break;
            }

            return result;
        }

        public struct function order(required string globalName, required array subscripts) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };            

            var ord = this.call_rest("order", postBody).order;
            var result = {};

            if(ord is "") {
                result.lastResult = true;
                result.value = "";
            }
            else {
                result.lastResult = false;
                result.value = ord;
            }

            return result;
        }

        public string function query(required string globalRef) output=false
        {
            return this.mumps_function("QUERY^KBBMCIDT", [arguments.globalRef]);
        }

        public numeric function merge(required string inputGlobalName,
                                      required array inputGlobalSubscripts,
                                      required string outputGlobalName,
                                      required array outputGlobalSubscripts) output=false
        {            
            var postBody = {
                fromGlobal: arguments.inputGlobalName,
                fromSubscripts: arguments.inputGlobalSubscripts,
                toGlobal: arguments.outputGlobalName,
                toSubscripts: arguments.outputGlobalSubscripts
            };

            var res = this.call_rest("merge", postBody);

            return 0;
        }

        public boolean function lock(required string globalName, required array subscripts, required numeric timeout=0) output=false
        {

            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts,
                timeout: arguments.timeout
            };

            return this.call_rest("lock", postBody).success;            

        }

        public boolean function unlock(required string globalName, required array subscripts) output=false
        {
            
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };

            return this.call_rest("unlock", postBody).success;
        }

        public string function version() output=false
        {
            return this.call_rest("version", {}).version;
        }

        public numeric function pid() output=false
        {
            return this.call_rest("pid", {}).pid;
        }

        public string function get_horolog() output=false
        {
            return this.mumps_function("GETHOROLOG^KBBMCIDT", [], false);
        }

        public string function convert_horolog(required string horolog) output=false
        {
            return this.mumps_function("CVTHOROLOG^KBBMCIDT", [arguments.horolog], false);
        }

        public struct function get_object(required string globalName, required array subscripts) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts
            };

            return this.call_rest("getobject", postBody).value;
        }

        public void function set_object(required string globalName, required array subscripts, required struct inputObject) output=false
        {
            var postBody = {
                global: arguments.globalName,
                subscripts: arguments.subscripts,
                object: arguments.inputObject
            };

            this.call_rest("setobject", postBody);

            return;
        }

        public any function mumps_function(required string fn, required array args, boolean autoRelink) output=false
        {
            var postBody = {
                function: arguments.fn,
                arguments: arguments.args,
                autoRelink: arguments.autoRelink
            };

            return this.call_rest("function", postBody).value;
        }

        public boolean function mumps_procedure(required string procedure, required array args, boolean autoRelink) output=false
        {
            var postBody = {
                procedure: arguments.procedure,
                arguments: arguments.args,
                autoRelink: arguments.autoRelink
            };

            try {
                var result = this.call_rest("procedure", postBody);
                return true;
            }
            catch(any ex) {
                return false;
            }

        }

}