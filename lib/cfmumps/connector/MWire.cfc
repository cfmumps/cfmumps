/*
 * CFMumps
 *
 * lib.cfmumps.connector.MWire:		CFMumps connector for GT.M and Cache (M/Wire)
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 19 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=false implements=lib.cfmumps.IMumpsConnector extends=lib.cfmumps.Admin {

	this.connectorName = "M/Wire Connector";
	this.connectorVersion = "0.0.5";
	this.admin = createObject("component", "lib.cfmumps.Admin");
	this.host = this.admin.getConfig("lib.cfmumps.connector.MWire", "host");
	this.port = val(this.admin.getConfig("lib.cfmumps.connector.MWire", "port"));
	this.password = this.admin.getConfig("lib.cfmumps.connector.MWire", "password");
	secureVal = this.admin.getConfig("lib.cfmumps.connector.MWire", "secure");

	this.implementsNativeGetObject = false;
	this.implementsNativeSetObject = false;

	if(secureVal is "true") {
		this.secure = true;
	}
	else {
		this.secure = false;
	}

	this.logLevel = this.admin.getConfig("lib.cfmumps.connector.MWire", "logLevel");
	this.authenticated = false;
	this.pIsOpen = false;


	public void function getSettings() output=true
	{
		include "settings/MWire.cfm";
	}

	public void function saveSettings() output=true
	{
		include "settings/MWire.cfm";
	}

	public component function open(boolean autoRelink) output=false
	{
		this.socket = createObject("component", "lib.TCPSocket");
		this.socket.open(this.host, this.port);

		if(this.secure) {
			this.socket.send("AUTH " & this.password);
			if(this.socket.success) {
				authReply = this.socket.read();
				if(authReply.success) {
					statusCode = left(authReply.response, 1);
					statusMessage = mid(authReply.response, 2);

					if(statusCode is "-") {
						resp.lines = 1;
						resp.type = "errorMessage";
						resp.message = statusMessage;
						resp.bytes = len(statusMessage);
						resp.success = false;
					}

					if(statusCode is "+") {
						resp.lines = 1;
						resp.type = "success";
						resp.message = statusMessage;
						resp.bytes = len(statusMessage);
						resp.success = true;
						resp.authenticated = true;
					}
				}
				else {
					resp.success = false
				}
			}
			else {
				resp.success =false
			}
		}
		else {
			this.authenticated = true;
		}

		this.pIsOpen = true;

		return this;
	}

	public component function close() output=false
	{
		this.authenticated = false;
		this.socket.close();
		this.pIsOpen = false;

		return this;
	}

	public boolean function isOpen() output=false
	{
		return this.pIsOpen;
	}

	public struct function sendMWire(required string message, string messageAddendum) output=false
	{
		if(this.logLevel >= 3) {
			writeLog(text="m/wire command: " & message, file="lib.cfmumps.connectors.MWire", type="Information");
		}

		var resp = {};

		resp.lines = 0;
		resp.type = "";
		resp.message = "";
		resp.bytes = 0;
		resp.success = true;
		resp.data = "";
		resp.dataType = "";

		if(this.authenticated) {
			if(isDefined("arguments.messageAddendum")) {
				fullMsg = arguments.message & chr(13) & chr(10) & arguments.messageAddendum;
			}
			else {
				fullMsg = arguments.message;
			}
			this.socket.send(fullMsg);

			if(find(" ", arguments.message)) {
				mWireCommand = rtrim(left(arguments.message, find(" ", arguments.message)));
			}
			else {
				mWireCommand = trim(arguments.message);
			}

			switch(mWireCommand) {
				case "SET":
					this.socket.read();
					this.socket.read();
					resp.data = "";
					resp.success = true;

					break;
				case "GET":
					this.socket.read();
					firstLine = this.socket.response;
					resp.data = firstLine;
					numBytes = int(mid(firstLine, 2));

					if(numBytes is -1) {
						resp.defined = false;
					}
					else {
						resp.defined = true;
					}

					if(numBytes > 0) {
						this.socket.read();
						resp.success = true;
						resp.data = this.socket.response;
					}
					else {
						if(resp.defined) {
							this.socket.read();
						}
						resp.data = "";
					}

					break;
				case "KILL":
					this.socket.read();
					resp.data = "";
					resp.success = true;

					break;
				case "DATA":
					resp.data = {};
					this.socket.read();
					defined = mid(this.socket.response, 2);

					switch(defined) {
						case "0":
							resp.data.defined = false;
							resp.data.hasData = false;
							resp.data.hasSubscripts = false;

							break;
						case "1":
							resp.data.defined = true;
							resp.data.hasData = true;
							resp.data.hasSubscripts = false;

							break;
						case "10":
							resp.data.defined = true;
							resp.data.hasData = false;
							resp.data.hasSubscripts = true;

							break;
						case 11:
							resp.data.defined = true;
							resp.data.hasData = true;
							resp.data.hasSubscripts = true;

							break;
						default:
							throw(type="mwire",
							      message="DATA received an invalid response",
							      errorCode="DATA_INVALID",
							      extendedInfo="Response Received: " & this.socket.response,
							      detail="M/Wire command: '" & arguments.message & "'");

							break;
					}

					break;
				case "ORDER":
					this.socket.read();
					defined = mid(this.socket.response, 2);

					if(defined is -1) {
						resp.lastResult = true;
						resp.data = "";
					}
					else {
						resp.lastResult = false;
						this.socket.read();
						resp.data = this.socket.response;
					}

					break;
				case "QUERY":
					this.socket.read();
					defined = mid(this.socket.response, 2);

					if(defined is -1) {
						resp.lastResult = true;
						resp.data = "";
					}
					else {
						resp.lastResult = false;
						this.socket.read();
						resp.data = this.socket.response;
					}
					break;
				case "LOCK":
					this.socket.read();
					success = mid(this.socket.response, 2);

					if(success is 1) {
						resp.success = true;
					}
					else {
						resp.success = false;
					}

					break;
				case "UNLOCK":
					this.socket.read();
					resp = {};

					break;
				case "MVERSION":
					this.socket.read();

					resp.data = mid(this.socket.response, 2);
					break;
				case "FUNCTION":
					this.socket.read();
					this.socket.read();

					resp.data = this.socket.response;

					break;
				case "PROCESSID":
					this.socket.read();
					resp.data = val(mid(this.socket.response, 2));
					break;
			}
		}

		return resp;
	} /* sendMWire() */


	public void function set(required string globalName, required array subscripts, required string value) output=false
	{
		mwCmd = "SET " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts) & " " & len(value);
		this.sendMWire(mwCmd, value);
	}

	public any function get(required string globalName, required array subscripts) output=false
	{
		mwCmd = "GET " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts);
		resp = this.sendMWire(mwCmd);

		return resp.data;
	}

	public numeric function increment(required string globalName, required array subscripts, required numeric count=1) output=false
	{
		var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

		return this.mumps_function("INCREMENT^KBBMCIDT", [glRef, count], false);
	}

	public void function kill(required string globalName, required array subscripts) output=false
	{
		mwCmd = "KILL " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts);
		this.sendMWire(mwCmd);
	}

	public struct function data(required string globalName, required array subscripts) output=false
	{
		mwCmd = "DATA " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts);
		resp = this.sendMWire(mwCmd);

		return resp.data;
	}

	public struct function order(required string globalName, required array subscripts) output=false
	{
		mwCmd = "ORDER " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts);
		resp = this.sendMWire(mwCmd);
		ret = {lastResult: resp.lastResult,
			   value: resp.data};

		return ret;
	}

	public string function query(required string globalRef) output=false
	{
		mwCmd = "QUERY " & arguments.globalRef;
		mwCmd = replace(mwCmd, "(", "[", "all");
		mwCmd = replace(mwCmd, ")", "]", "all");

		resp = this.sendMWire(mwCmd);

		resp.data = replace(resp.data, "[", "(", "all");
		resp.data = replace(resp.data, "]", ")", "all");

		if(len(resp.data) > 0) {
			var retVal = "^" & resp.data;
		}
		else {
			var retVal = "";
		}

		return retVal;
	}

	public numeric function merge(required string inputGlobalName,
								  required array inputGlobalSubscripts,
								  required string outputGlobalName,
								  required array outputGlobalSubscripts) output=false
	{
		var fromGlob = new lib.cfmumps.Global(inputGlobalName, inputGlobalSubscripts);
		var fromObj = fromGlob.getObject();
		var toGlob = new lib.cfmumps.Global(outputGlobalName, outputGlobalSubscripts);
		toGlob.setObject(fromObj);
		fromGlob.close();
		toGlob.close()

		return 1;
	}

	public boolean function lock(required string globalName,
	                             required array subscripts,
	                             required numeric timeout=0) output=false
	{
		mwCmd = "LOCK " & arguments.globalName & " " & arguments.timeout;
		resp = this.sendMWire(mwCmd);

		return resp.success;
	}

	public boolean function unlock(required string globalName, required array subscripts) output=false
	{
		mwCmd = "UNLOCK " & this.normalizeSubscripts(arguments.globalName, arguments.subscripts);
		this.sendMWire(mwCmd);

		return true;
	}

	public string function version() output=false
	{
		return this.sendMWire("MVERSION").data;
	}

	public any function mumps_function(required string fn,
	                              required array args,
	                              boolean autoRelink) output=false
	{
		if(arguments.args.len() > 0) {
			mwCmd = "FUNCTION " & arguments.fn & "(" & arrayToList(arguments.args) & ")";
		}
		else {
			mwCmd = "FUNCTION " & arguments.fn;
		}

		return this.sendMWire(mwCmd).data;
	}

	public numeric function pid() output=false
	{
		return this.sendMWire("PROCESSID").data;
	}

	public string function get_horolog() output=false
    {
        return this.mumps_function("GETHOROLOG^KBBMCIDT", [], false);
    }

    public string function convert_horolog(required string horolog) output=false
    {
        return this.mumps_function("CVTHOROLOG^KBBMCIDT", [arguments.horolog], false);
    }

	public boolean function mumps_procedure(required string procedure, required array args, boolean autoRelink) output=false
	{
		try {
			return true;


		}
		catch (any exception) {
			return false;
		}

	}

	public string function mWireVersion() output=false
	{
		return this.sendMWire("VERSION").data;
	}

	public string function normalizeSubscripts(required string globalName, required array subscripts) output=false
	{
		if(subscripts.len() is 0) {
			return arguments.globalName;
		}

		var os = arguments.globalName & "[";

		for(sub in arguments.subscripts) {
			if(isNumeric(sub)) {
				os &= sub & ",";
			}
			else {
				os &= chr(34) & sub & chr(34) & ","
			}
		}

		os = left(os, len(os) - 1) & "]";

		return os;
	}
}