/*
 * CFMumps
 *
 * lib.cfmumps.connector.CacheExtreme:		CFMumps connector for Cache Extreme
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 19 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=false implements=lib.cfmumps.IMumpsConnector extends=lib.cfmumps.Admin {

	this.connectorName = "Cache Connector";
	this.connectorVersion = "0.0.4-ALPHA";
	this.admin = createObject("component", "lib.cfmumps.Admin");
	this.logLevel = val(this.admin.getConfig("lib.cfmumps.connector.CacheExtreme", "logLevel"));
	this.routinePath = this.admin.getConfig("lib.cfmumps.connector.CacheExtreme", "routineOutputPath");
	this.namespace = this.admin.getConfig("lib.cfmumps.connector.CacheExtreme", "namespace");
	this.user = this.admin.getConfig("lib.cfmumps.connector.CacheExtreme", "user");
	this.password = this.admin.getConfig("lib.cfmumps.connector.CacheExtreme", "password");

	this.implementsNativeGetObject = false;
	this.implementsNativeSetObject = false;

	public void function getSettings() output=true
	{
		include "settings/CacheExtreme.cfm";
	}

	public void function saveSettings() output=true
	{
		include "settings/CacheExtreme.cfm";
	}

	public component function open(boolean autoRelink) output=false
	{
		this.cache = createObject("java", "com.coherentlogic.cfmumps.intersystems.Cache");
		this.cache.open(this.namespace, this.user, this.password, this.logLevel);

		return this;
	}

	public boolean function isOpen() output=false
	{
		return this.cache.isOpen();
	}

	public component function close() output=false
	{
		this.cache.close();

		return this;
	}

	public void function set(required string globalName, required array subscripts, required string value) output=false
	{
		this.cache.set(this.normalizeGlRef(arguments.globalName, arguments.subscripts, false), arguments.value);
	}

	public any function get(required string globalName, required array subscripts) output=false
	{
		return this.cache.get(this.normalizeGlRef(arguments.globalName, arguments.subscripts, false));
	}

	public numeric function increment(required string globalName, required array subscripts, required numeric count=1) output=false
	{
		var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

		return this.mumps_function("INCREMENT^KBBMCIDT", [glRef, count], false);
	}	

	public void function kill(required string globalName, required array subscripts) output=false
	{
		this.cache.kill(this.normalizeGlRef(arguments.globalName, arguments.subscripts, false));
	}

	public struct function data(required string globalName, required array subscripts) output=false
	{
		var defined = this.cache.data(this.normalizeGlRef(arguments.globalName, arguments.subscripts, false));
		var resp = {};

		switch(defined) {
			case "0":
				resp.defined = false;
				resp.hasData = false;
				resp.hasSubscripts = false;

				break;
			case "1":
				resp.defined = true;
				resp.hasData = true;
				resp.hasSubscripts = false;

				break;
			case "10":
				resp.defined = true;
				resp.hasData = false;
				resp.hasSubscripts = true;

				break;
			case "11":
				resp.defined = true;
				resp.hasData = true;
				resp.hasSubscripts = true;

				break;
		}

		return resp;
	}

	public struct function order(required string globalName, required array subscripts) output=false
	{
		var ord = this.cache.order(this.normalizeGlRef(arguments.globalName, arguments.subscripts, true));
		var result = {};

		if(ord == "") {
			result.lastResult = true;
			result.value = "";
		}
		else {
			result.lastResult = false;
			result.value = ord;
		}

		return result;
	}

	public string function query(required string globalRef) output=false
	{
		return this.cache.query(arguments.globalRef);
	}

	public numeric function merge(required string inputGlobalName,
	                              required array inputGlobalSubscripts,
	                              required string outputGlobalName,
	                              required array outputGlobalSubscripts) output=false
	{
		var inputGlRef = this.normalizeGlRef(arguments.inputGlobalName, arguments.inputGlobalSubscripts, false);
		var outputGlRef = this.normalizeGlRef(arguments.outputGlobalName, arguments.outputGlobalSubscripts, false)

		return this.cache.merge(inputGlRef, outputGlRef);
	}

	public boolean function lock(required string globalName, required array subscripts, required numeric timeout=0) output=false
	{
		var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);
		var status = this.cache.lock(glRef, timeout);

		if(status is 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean function unlock(required string globalName, required array subscripts) output=false
	{
		var glRef = this.normalizeGlRef(arguments.globalName, arguments.subscripts, false);

		try {
			this.cache.unlock(glRef);
			return true;
		}
		catch(any exception) {
			return false;
		}
	}

	public string function version() output=false
	{
		return this.cache.version();
	}

	public numeric function pid() output=false
	{
		return this.cache.pid();
	}

	public string function get_horolog() output=false
    {
        return this.mumps_function("GETHOROLOG^KBBMCIDT", [], false);
    }

    public string function convert_horolog(required string horolog) output=false
    {
        return this.mumps_function("CVTHOROLOG^KBBMCIDT", [arguments.horolog], false);
    }	

	public any function mumps_function(required string fn, required array args, boolean autoRelink) output=false
	{
		return this.cache.function(this.normalizeFunctionCall(arguments.fn, arguments.args));
	}

	public boolean function mumps_procedure(required string procedure, required array args, boolean autoRelink) output=false
	{
		return this.cache.procedure(this.normalizeFunctionCall(arguments.procedure, arguments.args))

		return false;
	}

	public string function normalizeGlRef(required string globalName, required array subscripts, boolean allowEmptyLastSubscript=false) output=false
	{
		var subscriptCount = 0;

		var retVal = "^" & arguments.globalName;
		if(subscripts.len() > 0) {
			retVal &= "(";

			for(sub in arguments.subscripts) {
				subscriptCount++;

				if(isNumeric(sub)) {
					retVal &= sub & ",";
				}
				else {
					if(sub is "") {
						if(subscriptCount != subscripts.len()) {
							throw(message = "Subscripts must not be empty.");
						}
						else {
							if(not arguments.allowEmptyLastSubscript) {
								throw(message = "Subscripts must not be empty.");
							}
						}
					}
					retVal &= chr(34) & sub & chr(34) & ",";
				}
			}
			retVal = left(retVal, len(retVal) - 1) & ")";
		}

		return retVal;
	}

	public string function normalizeFunctionCall(required string functionName, required array functionArgs) output=false
	{
		var retVal = functionName;

		if(functionArgs.len() > 0) {
			retVal &= "(";

			for(arg in arguments.functionArgs) {
				if(isNumeric(arg)) {
					retVal &= arg & ",";
				}
				else {
					retval &= chr(34) & arg & chr(34) & ",";
				}
			}
			retVal = left(retVal, len(retVal) - 1) & ")";
		}

		return retVal;
	}

}
