/*
 * CFMumps
 *
 * lib.cfmumps.Global:		CFMumps global API
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 16 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/


component displayName=global output=false {

	this.globalName = "";
	this.subscripts = [];
	this.atomic = true;
	this.lockTimeout = 5;

	public lib.cfmumps.global function init(required string globalName,
	                                        required array subscripts,
	                                        boolean atomic=true,
	                                        numeric lockTimeout=5) output=false
	{
		this.atomic = arguments.atomic;
		this.lockTimeout = arguments.lockTimeout;
		this.globalName = arguments.globalName;

		if(arguments.subscripts.len() > 0) {
			this.subscripts = arguments.subscripts;
		}
		else {
			this.subscripts = [];
		}

		this.db = new lib.cfmumps.Mumps();
		this.db.open();

		return this;
	} /* open() */

	public boolean function close() output=false
	{
		if(this.db.isOpen()) {
			this.globalName= "";
			this.subscripts = [];
			this.db.close();

			return true;
		}
		else {
			return false;
		}

	} /* close() */

	public any function value(string newValue) output=false
	{
		if(isDefined("arguments.newValue")) {
			this.db.set(this.globalName, this.subscripts, arguments.newValue);
		}

		return this.db.get(this.globalName, this.subscripts);
	} /* value() */

	public struct function defined() output=false
	{
		return this.db.data(this.globalName, this.subscripts);
	} /* defined() */

	public void function delete() output=false
	{
		this.db.kill(this.globalName, this.subscripts);
	} /* delete() */


	public void function set_object(required any inputStruct) output=false
	{
		this.setObject(arguments.inputStruct);
	}

	public void function setObject(required any inputStruct) output=false
	{
		if(this.atomic) {
			if(!this.db.lock(this.globalName, this.subscripts, this.lockTimeout)) {
				throw(message="Atomic setObject() failed to acquire lock on requested global node.");
			}
			else {
				if(this.db.implementsNativeSetObject) {
					this.db.connector.set_object(this.globalName, this.subscripts, arguments.inputStruct);
				}
				else {
					this.setObjectX(arguments.inputStruct);
				}
				this.db.unlock(this.globalName, this.subscripts);
			}
		}
		else {
			if(this.db.implementsNativeSetObject) {
				this.db.connector.set_object(this.globalName, this.subscripts, arguments.inputStruct);
			}
			else {
				this.setObjectX(arguments.inputStruct);
			}			
		}
	} /* setObject() */

	public void function setObjectX(required any inputStruct, array subscripts) output=false
	{
		if(isDefined("arguments.subscripts")) {
			subs = duplicate(arguments.subscripts);
		}
		else {
			subs = duplicate(this.subscripts);
		}

		if(isStruct(arguments.inputStruct)) {
			var keyList = structKeyList(arguments.inputStruct);
			var keyArray = listToArray(keyList);

			for(key in keyArray) {
				arrayAppend(subs, key);

				if(isStruct(arguments.inputStruct[key]) or isArray(arguments.inputStruct[key])) {
					this.setObjectX(arguments.inputStruct[key], subs);
				}
				else {
					this.db.set(this.globalName, subs, arguments.inputStruct[key]);
				}

				arrayDeleteAt(subs, arrayLen(subs));
			}
		}
		else if(isArray(arguments.inputStruct)) {
			for(index = 1; index < arrayLen(arguments.inputStruct); index++) {
				arrayAppend(subs, index);

				if(isStruct(arguments.inputStruct[index]) or isArray(arguments.inputStruct[index])) {
					this.setObjectX(arguments.inputStruct[index], subs);
				}
				else {
					this.db.set(this.globalName, subs, arguments.inputStruct[index]);
				}

				arrayDeleteAt(subs, arrayLen(subs));
			}
		}

		return;
	} /* setObjectX() */


	public struct function get_object() output=false
	{
		return this.getObject();
	}

	public struct function getObject() output=false
	{
		if(this.atomic) {
			if(!this.db.lock(this.globalName, this.subscripts, this.lockTimeout)) {
				throw(message="Atomic getObject() failed to acquire lock on requested global node.");
			}
			else {
				if(this.db.implementsNativeGetObject) {
					var retVal = this.db.connector.get_object(this.globalName, this.subscripts);
				}
				else {
					var retVal = this.getObjectX();
				}

				this.db.unlock(this.globalName, this.subscripts);
				return retVal;
			}
		}
		else {
			if(this.db.implementsNativeGetObject) {
				var retVal = this.db.connector.get_object(this.globalName, this.subscripts);
			}
			else {
				var retVal = this.getObjectX();
			}			
		}
	} /* getObject() */

	public struct function getObjectX(array subscripts, struct outputStruct) output=false
	{
		if(isDefined("arguments.subscripts")) {
			var mSubscripts = duplicate(arguments.subscripts);
		}
		else {
			var mSubscripts = duplicate(this.subscripts);
		}

		if(!isDefined("arguments.outputStruct")) {
			var outStruct = {};
		}
		else {
			var outStruct = duplicate(arguments.outputStruct);
		}

		var lastResult = false;
		arrayAppend(mSubscripts, "");

		while(!lastResult) {
			var order = this.db.order(this.globalName, mSubscripts);
			var lastResult = order.lastResult;

			if(lastResult) {
				continue;
			}

			arrayDeleteAt(mSubscripts, arrayLen(mSubscripts));
			arrayAppend(mSubscripts, order.value);

			var structSubs = arraySlice(mSubscripts, arrayLen(this.subscripts) + 1, arrayLen(mSubscripts) - arrayLen(this.subscripts));
			var data = this.db.data(this.globalName, mSubscripts);

			if(data.hasData and data.hasSubscripts) {
				assignment = "outStruct";
				for(elem in structSubs) {
					if(isNumeric(elem)) {
						assignment &= "[" & elem & "]";
					}
					else {
						assignment &= "['" & elem & "']";
					}
				} /* for(elem in structSubs) */

				setVariable("#assignment#['']", this.db.get(this.globalName, mSubscripts));
				structAppend(outStruct, this.getObjectX(mSubscripts, outStruct));
			}
			else if(data.hasSubscripts and not data.hasData) {
				structAppend(outStruct, this.getObjectX(mSubscripts, outStruct));
			}
			else if(data.hasData and not data.hasSubscripts) {
				assignment = "outStruct";
				for(elem in structSubs) {
					if(isNumeric(elem)) {
						assignment &= "[" & elem & "]";
					}
					else {
						assignment &= "['" & elem & "']";
					}
				} /* for(elem in structSubs) */

				var nodeValue = this.db.get(this.globalName, mSubscripts);

				try {
					if(isNumeric(nodeValue)) {
						nodeValue = lsParseNumber(nodeValue);
					}
				}
				catch(any ex) {
					// do nothing
				}

				setVariable("#assignment#", nodeValue);
			}
		} /* while(!lastResult) */

		return outStruct;
	} /* getObjectX() */

}