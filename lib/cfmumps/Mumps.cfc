/*
 * CFMumps
 *
 * lib.cfmumps.Mumps:		CFMumps Basic API
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 16 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component output=true extends=lib.cfmumps.Admin {

	public lib.cfmumps.Mumps function init(component connector) output=false
	{
		if(!isDefined("arguments.connector")) {
			this.connector = "";
			this.component = getProfileString(expandPath("/lib/cfmumps/cfmumps.ini"), "General", "connector");
			this.connector = createObject("component", this.component);			
		}
		else {
			this.connector = arguments.connector;
		}

		this.verifyConnector(this.connector);
		
		this.implementsNativeGetObject = this.connector.implementsNativeGetObject;
		this.implementsNativeSetObject = this.connector.implementsNativeSetObject;
		
		return this;
	}

	public lib.cfmumps.Mumps function open() output=false
	{
		this.connector.open();

		return this;
	}

	public boolean function isOpen() output=false
	{
		return this.connector.isOpen();
	}

	public lib.cfmumps.Mumps function close() output=false
	{
		this.connector.close();

		return this;
	}

	public void function set(required string globalName,
							 required array subscripts,
							 required string value) output=false
	{
		this.connector.set(globalName, subscripts, value);
	}

	public any function get(required string globalName,
							required array subscripts) output=false
	{
		return this.connector.get(globalName, subscripts);
	}

	public numeric function increment(required string globalName,
									  required array subscripts,
									  required numeric count=1) output=false
	{
		return this.connector.increment(globalName, subscripts, count);
	}

	public void function kill(required string globalName,
							  required array subscripts) output=false
	{
		this.connector.kill(globalName, subscripts);
	}

	public struct function data(required string globalName,
							    required array subscripts) output=false
	{
		return this.connector.data(globalName, subscripts);
	}

	public struct function order(required string globalName,
							     required array subscripts) output=false
	{
		return this.connector.order(globalName, subscripts);
	}

	public string function query(required string globalRef) output=false
	{
		return this.connector.query(globalRef);
	}

	public numeric function merge(required string inputGlobalName,
								  required array inputGlobalSubscripts,
								  required string outputGlobalName,
								  required array outputGlobalSubscripts) output=false
	{
		return this.connector.merge(inputGlobalName, inputGlobalSubscripts, outputGlobalName, outputGlobalSubscripts);
	}

	public boolean function lock(required string globalName,
								 required array subscripts,
								 required numeric timeout = 0) output=false
	{
		return this.connector.lock(globalName, subscripts, timeout);
	}

	public boolean function unlock(required string globalName,
								   required array subscripts) output=false
	{
		try {
			this.connector.unlock(globalName, subscripts);
			return true;
		}
		catch(any exception) {
			return false;
		}
	}

	public any function mumps_function(required string fn,
								 required array args,
								 boolean autoRelink) output=false
	{
		var alink = false;

		if(isDefined("arguments.autoRelink")) {
			if(arguments.autoRelink) {
				alink = true;
			}
		}

		return this.connector.mumps_function(arguments.fn, arguments.args, alink);
	}

	public struct function mumps_function_ex(required string fn, required array args, boolean autoRelink) output=false
	{
		return this.mumps_do_ex(arguments.fn, arguments.args, arguments.autoRelink, false);
	}

	public struct function mumps_procedure_ex(required string fn, required array args, boolean autoRelink) output=false
	{
		return this.mumps_do_ex(arguments.fn, arguments.args, arguments.autoRelink, true);
	}

	public struct function mumps_do_ex(required string fn, required array args, required boolean autoRelink, required boolean isProcedure)
	{
		var alink = false;

		if(isDefined("arguments.autoRelink")) {
			if(arguments.autoRelink) {
				alink = true;
			}
		}

		var fnId = createUUID();
		var argNum = 0;

		var arg = "";
		
		for(arg in arguments.args) {
			if(!isStruct(arg)) {
				if(isNumeric(arg)) {
					this.mumps_procedure("ADDARG^KBBMCIDT", [fnId, argNum, "number", arg]);
				}
				else {
					this.mumps_procedure("ADDARG^KBBMCIDT", [fnId, argNum, "string", arg]);
				}
			}
			else {
				if(!structKeyExists(arg, "type")) throw("must pass 'type' property for each argument (string, number, varnam, varref)");
				if(!structKeyExists(arg, "data")) throw("must pass 'data' property for each argument");

				switch(arg.type) {
					case 'string': 
					case 'number': 
					case 'byval': 
					case 'byref':
						break; 
					default: throw("'type' property must be one of 'string', 'number', 'varnam', or 'varref'");
				}
				
				this.mumps_procedure("ADDARG^KBBMCIDT", [fnId, argNum, arg.type, arg.data]);
			}

			argNum++;
		}

		var proc = 0;
		if(arguments.isProcedure) proc = 1;

		var fnResult = this.mumps_function("EXEC^KBBMCIDT", [arguments.fn, fnId, alink, proc]);
		

		var symGbl = new lib.cfmumps.Global("KBBMOUT", [fnId]);
		var localSymbolTable = symGbl.getObject();

		symGbl.delete();
		symGbl.close();

		return {
			result: fnResult,
			locals: localSymbolTable
		};
	}

	public string function get_horolog() output=false
    {
        return this.connector.get_horolog();
    }

    public string function convert_horolog(required string horolog) output=false
    {
        return this.connector.convert_horolog(arguments.horolog);
    }


	public boolean function mumps_procedure(required string procedure,
								   	  required array args,
								      boolean autoRelink) output=false
	{
		var alink = false;

		if(isDefined("arguments.autoRelink")) {
			if(arguments.autoRelink) {
				alink = true;
			}
		}

		return this.connector.mumps_procedure(arguments.procedure, arguments.args, alink);
	}

	public string function mEval(required string mCode,
							     required string outputFile,
							     required string routineName) output=false
	{
		return this.connector.mEval(arguments.mCode, arguments.outputFile, arguments.routineName);
	}

	public string function version() output=false
	{
		return this.connector.version();
	}

	public numeric function pid() output=false
	{
		return this.connector.pid();
	}

}