/*
 * CFMumps
 *
 * lib.util:		CFMumps utility APIs
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 12 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component displayname=util accessors=true output=false persistent=false
{
	public any function getPiece(required string inputString,
								 required string delimiter,
								 required numeric pieceNumber) output=false
	{

		if(find(delimiter, inputString) > 0) {
			var oa = listToArray(inputString, delimiter, true);

			if(pieceNumber <= oa.len()) {
				var retVal = oa[pieceNumber];
			}
			else {
				var retVal = "";
			}
		}
		else {
			var retVal = inputString;
		}

		return retVal;
	}

	public string function formatJson(val) {
		var retval = '';
		var str = val;
		var pos = 0;
		var strLen = str.len();
		var indentStr = '&nbsp;&nbsp;&nbsp;&nbsp;';
		var newLine = '<br />';
		var char = '';

		for (var i=0; i<strLen; i++) {
			char = str.substring(i,i+1);

			if (char == '}' || char == ']') {
				retval = retval & newLine;
				pos = pos - 1;

				for (var j=0; j<pos; j++) {
					retval = retval & indentStr;
				}
			}

			retval = retval & char;	

			if (char == '{' || char == '[' || char == ',') {
				retval = retval & newLine;

				if (char == '{' || char == '[') {
					pos = pos + 1;
				}

				for (var k=0; k<pos; k++) {
					retval = retval & indentStr;
				}
			}
		}

		return retval;
	}

	public string function setPiece(required string inputString,
									required string delimiter,
									required numeric pieceNumber,
									required any newValue) output=false
	{
		var oa = listToArray(inputString, delimiter, true);

		if(pieceNumber <= oa.len()) {
			oa[pieceNumber] = newValue;
		}
		else {
			throw(message="Error in lib.cfmumps.Util.setPiece()",
				  detail="pieceNumber out of bounds");
		}

		return arrayToList(oa, delimiter);
	}

	/*
	 * FILEMAN DATE FORMAT:
	 *
	 * -Subtract 1700 from actual year, then:
	 *
	 * YYYMMDD.HHMM
	 **/
	public date function convertFromFmDate(required string filemanDate)	output=false
	{
		if(find(".", arguments.filemanDate)) {
			var timePart = this.getPiece(arguments.filemanDate, ".", 2);
			var datePart = this.getPiece(arguments.filemanDate, ".", 1);
		}
		else {
			var datePart = arguments.filemanDate;
		}

		var yearPart = val(left(datePart, 3));
		var realYear = yearPart + 1700;
		var monthPart = mid(datePart, 4, 2);
		var dayPart = mid(datePart, 6, 2);

		if(isDefined("timePart")) {
			var hourPart = val(left(timePart, 2));
			var minPart = val(mid(timePart, 3, len(timePart) - 2));

			hourStr = numberFormat(hourPart, "09");
			minStr = numberFormat(minPart, "09");

			var retVal = parseDateTime(monthPart & "/" & dayPart & "/" & realYear & " " & hourStr & ":" & minStr, "MM/dd/yyyy H:mm");
		}
		else {
			var retVal = parseDateTime(monthPart & "/" & dayPart & "/" & realYear);
		}

		return retVal;
	}

	public string function convertToFmDate(required date inputDate) output=false
	{
		var fmYear = numberFormat(datePart("yyyy", inputDate) - 1700, "009");
		var fmMonth = numberFormat(datePart("m", inputDate), "09");
		var fmDay = numberFormat(datePart("d", inputDate), "09");
		var fmHour = numberFormat(datePart("h", inputDate), "09");
		var fmMinute = numberFormat(datePart("n", inputDate), "09");

		var fmDate = fmYear & fmMonth & fmDay & "." & fmHour & fmMinute;

		return fmDate;
	}

	public string function titleCase(required string str) output=false
	{
		var caseCount = 1;
		var arr = listToArray(arguments.str, " ");

		for(elem in arr) {
			if(refind("^[a-z]", left(elem, 1))) {
				if(caseCount is 1) {
					if(len(elem) > 1) {
						arguments.str = ucase(left(elem, 1)) & right(elem, len(elem) - 1) & " ";
					}
					else {
						arguments.str &= ucase(elem) & " ";
					}

				}
				else {
					if(len(elem) > 1) {
						arguments.str &= ucase(left(elem, 1)) & right(elem, len(elem) - 1) & " ";
					}
					else {
						arguments.str &= ucase(elem) & " ";
					}
				}
				caseCount++;
			}
		}

		return left(arguments.str, len(arguments.str) - 1);
	}

}
