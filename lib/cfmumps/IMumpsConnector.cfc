/*
 * CFMumps
 *
 * lib.cfmumps.IMumpsConnector:		CFMumps connector interface spec
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 19 Mar 2018
 *
 * Copyright (C) 2014, 2018 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

interface displayName="IMumpsConnector" {

	public component function open(boolean autoRelink) output=false;

	public boolean function isOpen() output=false;

	public component function close() output=false;

	public void function getSettings() output=true;

	public void function saveSettings() output=true;

	public void function set(required string globalName,
						     required array subscripts,
						     required string value) output=false;

	public any function get(required string globalName,
	                        required array subscripts) output=false;

	public numeric function increment(required string globalName,
									  required array subscripts,
									  required numeric count=1) output=false;

	public void function kill(required string globalName,
	                          required array subscripts) output=false;

	public struct function data(required string globalName,
	                          required array subscripts) output=false;

	public struct function order(required string globalName,
	                             required array subscripts) output=false;

	public string function query(required string globalRef) output=false;

	public numeric function merge(required string inputGlobalName,
								  required array inputGlobalSubscripts,
								  required string outputGlobalName,
								  required array outputGlobalSubscripts) output=false;

	public boolean function lock(required string globalName,
								 required array subscripts,
								 required numeric timeout = 0) output=false;

	public boolean function unlock(required string globalName,
								   required array subscripts) output=false;

	public string function version() output=false;

	public any function mumps_function(required string fn,
								  required array args,
								  boolean autoRelink) output=false;

	public boolean function mumps_procedure(required string procedure,
								   	   required array args,
								       boolean autoRelink) output=false;

	public numeric function pid() output=false;

	public string function get_horolog() output=false;

	public string function convert_horolog(required string horolog) output=false;

}