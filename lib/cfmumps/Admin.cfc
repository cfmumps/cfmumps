/*
 * CFMumps
 *
 * lib.admin:		CFMumps admin support component
 *
 * Author:   John P. Willis <jpw@coherent-logic.com>
 * Modified: 10 Jan 2017
 *
 * Copyright (C) 2014, 2017 Coherent Logic Development LLC
 *
 * This file is part of CFMumps.
 *
 * CFMumps is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFMumps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

component displayname=Admin extends=lib.cfmumps.Util accessors=true output=false persistent=false
{
	public lib.cfmumps.Admin function init() output=false
	{
		return this;
	}

	public string function getConfig(required string section, required string key) output=false
	{
		var iniPath = expandPath("/lib/cfmumps/cfmumps.ini");

		return getProfileString(iniPath, section, key);
	}

	public void function setConfig(required string section, required string key, required string value) output=false
	{
		var iniPath = expandPath("/lib/cfmumps/cfmumps.ini");

		setProfileString(iniPath, section, key, value);
	}

	public void function verifyConnector(required component connector) output=false
	{
		var componentMeta = getMetaData(connector);

		if(componentMeta.extends.fullname != "lib.cfmumps.admin") {
			throw(message=componentMeta.fullname & ": Connector verification failed",
				  detail="Must extend lib.cfmumps.admin");
		}

		if(structKeyExists(componentMeta, "implements")) {
			var impStruct = componentMeta["implements"];

			if(!structKeyExists(impStruct, "lib.cfmumps.IMumpsConnector")) {
				throw(message=componentMeta.fullname & ": Connector verification failed",
					  detail="Must implement lib.cfmumps.IMumpsConnector");
			}
		}
		else {
			throw(message=componentMeta.fullname & ": Connector verification failed",
				  detail="Must implement lib.cfmumps.IMumpsConnector");
		}

		if(!isDefined("connector.connectorName")) {
			throw(message=componentMeta.fullname & ": Connector verification failed",
				  detail="No connectorName exposed");
		}

		if(!isDefined("connector.connectorVersion")) {
			throw(message=componentMeta.fullname & ": Connector verification failed",
				  detail="No connectorVersion exposed")
		}

	}

}