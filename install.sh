#!/usr/bin/env bash

#
# CFMumps
#
# install: CFMumps installer driver
#
# Author:   John P. Willis <jpw@coherent-logic.com>
# Modified: 30 Jan 2017
#
# Copyright (C) 2014, 2017 Coherent Logic Development LLC
#
# This file is part of CFMumps.
#
# CFMumps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFMumps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with CFMumps.  If not, see <http://www.gnu.org/licenses/>.
#

PGM=$(basename $0)

function usage
{
    
    exit 1
}

function detect_os
{
    if [[ ! -f "/etc/os-release" ]]
    then
        echo "Unsupported operating system or operating system version."
        exit 2
    fi

    source /etc/os-release

    case "$NAME" in
        Ubuntu|Debian)
            OS="debian"
            ;;

        *)
            echo "Unsupported operating system or operating system version."
            exit 2
    esac

    OS_VERSION="$VERSION_ID"
}

function detect_cf
{

}

function detect_lucee
{

}

function detect_acf
{

}

function build_environment
{

}

function build_gtmji
{

}

function detect_mumps
{

}

function detect_gtm
{

}

function detect_cache
{

}


TEMP=$(getopt -o h --long distro:,connector:,cfml:,mwire-host:,mwire-port:,mwire-password:,mwire-secure,gtm-path:,cache-path:,cache-username:,cache-password:,cache-namespace:,gtm-global-directory:,webroot:,help -n '${PGM}' -- "$@")
eval set -- "$TEMP"

MWIRE_SECURE=0

while true
do
    case "$1" in
        -h,--help)
            usage
            ;;
        --connector)
            case "$2" in
                gtmji)
                    CONNECTOR="lib.cfmumps.connectors.GTMJI"
                    ;;
                cacheextreme)
                    CONNECTOR="lib.cfmumps.connectors.CacheExtreme"
                    ;;
                mwire)
                    CONNECTOR="lib.cfmumps.connectors.MWire"
                    ;;
                *)
                    echo "'$2' is not a valid CFMumps connector."
                    echo "CFMumps currently supports 'gtmji', 'cacheextreme', and 'mwire'."
                    usage
                    ;;
            esac
            shift 2
            ;;
        --cfml)
            case "$2" in
                lucee)
                    CFML_VERSION=lucee
                    ;;
                railo)
                    CFML_VERSION=railo
                    ;;
                adobe)
                    CFML_VERSION=adobe
                    ;;
                *)
                    echo "'$2' is not a valid CFML engine."
                    echo "CFMumps currently supports 'lucee', 'railo', and 'adodbe'."
                    usage
                    ;;
            esac
            shift 2
            ;;
        --mwire-host)
            MWIRE_HOST="$2"
            shift 2
            ;;
        --mwire-port)
            MWIRE_PORT="$2"
            shift 2
            ;;
        --mwire-password)
            MWIRE_PASSWORD="$2"
            shift 2
            ;;
        --mwire-secure)
            MWIRE_SECURE=1
            shift
            ;;
        --gtm-path)
            GTM_PATH="$2"
            shift 2
            ;;
        --cache-path)
            CACHE_PATH="$2"
            shift 2
            ;;
        --cache-username)
            CACHE_USERNAME="$2"
            shift 2
            ;;
        --cache-password)
            CACHE_PASSWORD="$2"
            shift 2
            ;;
        --cache-namespace)
            CACHE_NAMESPACE="$2"
            shift 2
            ;;
        --gtm-global-directory)
            GTM_GLOBAL_DIRECTORY="$2"
            shift 2
            ;;
        --webroot)
            WEBROOT="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option '$1'"
            usage
            ;;
    esac
done
        




