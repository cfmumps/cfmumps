#!/bin/bash

if [ -d /opt/lucee ]; then
    export CF_VER="lucee"
elif [ -d /opt/railo ]; then
    export CF_VER="railo"
fi    

echo -n "GT.M path? "
read gtm_dist


export gtm_dist
export JAVA_HOME=/opt/${CF_VER}/jdk/jre
export JAVA_SO_HOME=${JAVA_HOME}/jre/lib/amd64
export JVM_SO_HOME=${JAVA_HOME}/jre/lib/amd64/server

mkdir gtmji_install
pushd gtmji_install

wget http://www.coherent-logic.com/downloads/ji_plugin_63202_50208.tar.gz -O ./gtmji.tar.gz
tar zxvf gtmji.tar.gz

cd ji_plugin_63202_50208

make all
make test
sudo -E make install install-test
make clean

popd
rm -rf gtmji_install
