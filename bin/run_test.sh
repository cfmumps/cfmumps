#!/usr/bin/env bash

function usage
{
    echo ""
    echo "usage:  run_test.sh [--url=<base-url> | -u <base-url>] <test-name>"
    echo "        run_test.sh [--help | -h]"
    echo ""

    exit 1
}

function wget_status_handler
{
    local wget_status=$1

    if [[ $wget_status > 0 ]]
    then
	echo -e "\e[31m"
    fi
    
    case $wget_status in
	1)
	    echo "Generic error retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	2)
	    echo "Parse error retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	3)
	    echo "File I/O error retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	4)
	    echo "Network failure retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	5)
	    echo "SSL verification failure retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	6)
	    echo "Authentication failure retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	7)
	    echo "Protocol error retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
	8)
	    echo "Server issued error response retrieving ${URL}"
	    echo -e "\e[0m"
	    exit 1
	    ;;
    esac



}

VERBOSE=0

TEMP=$(getopt -o u:hv --long url:,help,verbose -n 'run_test.sh' -- "$@")
eval set -- "$TEMP"

while true
do
    case "$1" in
	-v|--verbose)
	    VERBOSE=1
	    shift
	    ;;
	-u|--url)
	    case "$2" in
		"")
		    usage
		    ;;
		*)
		    BASEURL="$2"
		    shift 2
		    ;;
	    esac
	    ;;
	-h|--help)
	    usage
	    ;;
	--)
	    shift
	    break
	    ;;
    esac
done

if [[ $# < 1 ]]
then
    usage
fi   
echo -e "\e[37m"

echo "CFMumps Testing Agent v0.0.1"
echo " Copyright (C) 2017 Coherent Logic Development"
echo ""

TOTALBUNDLES=$#
TOTALPASS=0
TOTALSPECS=0

echo -e "RUNNING ${TOTALBUNDLES} BUNDLES [$@]"

for TESTNAME in $@
do
    
    if [[ $VERBOSE == 1 ]]
    then
	URL="${BASEURL}/tests/${TESTNAME}.cfc?method=runRemote&reporter=text"
	echo -e "\e[95m"
	wget -q -O - ${URL}
	wget_status_handler $?
	echo -e "\e[37m"
	echo
    fi
    
    URL="${BASEURL}/tests/${TESTNAME}.cfc?method=runRemote&reporter=json"
    REPORT=$(wget -q -O - ${URL})
    wget_status_handler $?	    
    
    PASS=$(echo ${REPORT} | jq '.totalPass')
    SPECS=$(echo ${REPORT} | jq '.totalSpecs')

    TOTALPASS=$((TOTALPASS + PASS))
    TOTALSPECS=$((TOTALSPECS + SPECS))

    if [[ $PASS < $SPECS ]]
    then
	BUNSTAT="\e[31mFAIL\e[36m"
    else
	BUNSTAT="\e[32mPASS\e[36m"
    fi
    
    echo -e "\e[36m   ${TESTNAME}:  ${BUNSTAT} (${PASS} OF ${SPECS} PASSED)\e[37m"
done
echo
echo
echo -n "BUNDLES RUN:  ${TOTALBUNDLES}   SPECS TESTED:  ${TOTALSPECS}   SPECS PASSED:  ${TOTALPASS}   "

if [[ $TOTALPASS != $TOTALSPECS ]]
then
    echo -e "\e[31m[OVERALL FAIL]\e[37m"
    echo -e "\e[0m"
    exit 1
else
    echo -e "\e[32m[OVERALL PASS]\e[37m"
    echo -e "\e[0m"
    exit 0
fi

