#!/usr/bin/env bash

PGM=$(basename $0)

function usage
{
    echo "usage:  ${PGM} [-h|--help] | [-c GTMJI|CacheExtreme|MWire|REST] | [--connector=GTMJI|CacheExtreme|MWire|REST]"
    echo
    exit 1
}

CONNECTOR=""

TEMP=$(getopt -o c:h --long connector:,help -n 'set_connector.sh' -- "$@")
eval set -- "$TEMP"

while true
do
    case "$1" in
	-h|--help)
	    usage
	    ;;
	-c|--connector)
	    case "$2" in
		CacheExtreme)
		    CONNECTOR="CacheExtreme"
		    ;;
		GTMJI)
		    CONNECTOR="GTMJI"
		    ;;
		MWire)
		    CONNECTOR="MWire"
		    ;;
        REST)
            CONNECTOR="REST"
            ;;
		*)
		    echo "'$2' is not a valid CFMumps connector."
		    echo "Valid connectors are 'CacheExtreme', 'GTMJI', 'REST', and 'MWire'"
		    usage
		    ;;
	    esac
	    shift 2
	    ;;
	--)
	    shift
	    break
	    ;;
    esac
done

[[ "${CONNECTOR}" == "" ]] && usage

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

INI="${SCRIPTDIR}/../lib/cfmumps/cfmumps.ini.${CONNECTOR}"
NEWINI="${SCRIPTDIR}/../lib/cfmumps/cfmumps.ini"

unalias cp > /dev/null 2>&1
cp $INI $NEWINI

if [[ $? == 0 ]]
then
    echo "Connector set to ${CONNECTOR}"
    exit 0
else
    echo "Connector set FAILED"
    exit 1
fi   
