KBBMFS ;CLD/JPW - CLINICAL FLOWSHEETS;19 JUL 2014
;;0.01;KBBMFS;**PATCHES**;19 JUL 2014;Build 1
 Q
BIX N F,C,O,CN,CS,IX,DF,LN S (F,C,O,CN)="",LN="DEF",IX=$$GL(2),DF=$$GL(3),CS=$$GL(4) K @IX
    S F=$O(@DF@(F)) Q:F=""  S @IX@(@DF@(F))=F,C="" D
    .F  S C=$O(@DF@(F,C)) Q:C=""  S @IX@(@DF@(F),@DF@(F,C))=C,O="" D
    ..F  S O=$O(@DF@(F,C,O)) Q:O=""  S @IX@(@DF@(F),@DF@(F,C),@DF@(F,C,O))=O,CN=""  D
    ...F  S CN=$O(@DF@(F,C,O,CS,CN)) Q:CN=""  S @IX@(@DF@(F),@DF@(F,C),@DF@(F,C,O),CN)=@DF@(F,C,O,CS,CN,"label")
    Q
DEF ;^KBBMFSIX;^KBBMFS;Controls
    Q
GL(NM) S LN="DEF" Q $P($T(@LN),";",NM)